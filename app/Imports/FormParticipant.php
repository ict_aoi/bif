<?php

namespace App\Imports;

use DB;
use Carbon\Carbon;
use App\Models\Participant;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;

class FormParticipant implements WithMultipleSheets
{
    use WithConditionalSheets;

    public function conditionalSheets(): array
    {
        return [
            'active' => new ActiveSheetImport(),
        ];
    }
}

class ActiveSheetImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        $created_at = carbon::now()->todatetimestring();
        try {
            DB::beginTransaction();
            foreach ($rows as $row) {
                // dd($row);
                $nik                = $row['nik'];
                // $name               = $row['name'];
                // $password           = $row['password'];
                // $circle             = $row['circle'];
                // $email              = $row['email'];
                $name               = $row['name'];
                // $integration_type   = $row['integration_type'];

                $is_exists = Participant::where('nik', $nik)->exists();
                // $is_exists = Participant::where(DB::raw('lower(nik)'), strtolower($nik))->where('circle', $circle)->exists();
                // $absen = DB::connection('middleware_live')
                //     ->table('users')->where('nik', $nik)->first();

                // if (!$is_exists && isset($absen)) {
                //     if ($circle == 'PANITIA BIF') {
                //         $cek = Participant::where('nik', strtolower($nik))->first();
                //         if (isset($cek->circle)) {
                //             continue;
                //         }
                //     }
                //     $participant = Participant::Create([
                //         'nik'           => $absen->nik,
                //         'name'          => $absen->name,
                //         'factory'       => $absen->absen_factory,
                //         'circle'        => $circle,
                //         'position'      => $absen->position_name,
                //         'birth_day'     => (isset($absen->absen_password) && $absen->absen_password != null) ? $absen->absen_password : 000000,
                //         'created_at'    => $created_at,
                //         'updated_at'    => $created_at,
                //     ]);
                // }
                if (!$is_exists) {
                    
                    $participant = Participant::Create([
                        'nik'           => $nik,
                        'name'          => $name,
                        'factory'       => '-',
                        'circle'        => '',
                        'position'      => '-',
                        'peserta'       => '1',
                        'birth_day'     => 000000,
                        'email'         => '',
                        'created_at'    => $created_at,
                        'updated_at'    => $created_at,
                    ]);
                }
            }
            DB::commit();
        } catch (Exception $error) {
            DB::rollBack();
            return ResponseFormatter::error('Import failed', 422);
        }
    }
}
