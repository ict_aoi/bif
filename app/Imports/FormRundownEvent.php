<?php namespace App\Imports;

use DB;
use Carbon\Carbon;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;


use App\Models\RundownEvent;

class FormRundownEvent implements WithMultipleSheets
{
    use WithConditionalSheets;

    public function conditionalSheets(): array
    {
        return [
            'active' => new ActiveSheetImport(),
        ];
    }
}

class ActiveSheetImport implements ToCollection,WithHeadingRow
{
    public function collection(Collection $rows)
    {
        $created_at = carbon::now()->todatetimestring();
        try 
        {
            DB::beginTransaction();
            foreach ($rows as $row) 
            {
                $event_date    = $row['event_date'];
                $name           = $row['name'];
                $description    = $row['description'];
                $location       = $row['location'];

                RundownEvent::Create([
                    'event_date'    => $event_date,
                    'name'          => $name,
                    'description'   => $description,
                    'location'      => $location,
                    'created_at'    => $created_at,
                    'updated_at'    => $created_at,
                ]);
               
            }

            DB::commit();
        } catch (Exception $error) {
            DB::rollBack();
            return ResponseFormatter::error('Import failed',422);
        } 
    }
}
