<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $path = Config::get('storage.asset');
        return $this->subject('BINABUSANA IMPROVEMENT FORUM')
            ->view('emails.invitation')
            ->attach($path . '/6ry6DCiTWA7jHMkpn3X4LSWgbYVTER7b.png', [
                'as' => 'gambar.jpg',
                'mime' => 'image/jpeg',
            ]);
    }
}
