<?php

namespace App\Models;

use App\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Participant extends Model
{
    use HasFactory, Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'];
    protected $fillable = [
        'nik',
        'name',
        'birth_day',
        'lottery_number',
        'factory',
        'registration_at',
        'email',
        'phone_number',
        'created_at',
        'updated_at',
        'deleted_at',
        'circle',
        'position',
        'peserta',
    ];
    function safe_encode($string)
    {
        $data = str_replace(array('/'), array('_'), $string);
        return $data;
    }
    function safe_decode($string, $mode = null)
    {
        $data = str_replace(array('_'), array('/'), $string);
        return $data;
    }
    protected $appends = [
        'url_invitation_card', 'nomor',
    ];
    public function getNomorAttribute()
    {
        return substr($this->attributes['lottery_number'], 0, -1);
    }
    public function getUrlInvitationCardAttribute()
    {
        // $enc_nik = $this->safe_decode(Crypt::encryptString($this->attributes['nik']));
        // return route('invitationCard', $enc_nik);
        $nik = $this->attributes['nik'];
        return route('invitationCard', $nik);
        // return 'tes';
    }

    static function random_()
    {
        $try = 250;



        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");


            $referral_code  = Carbon::now()->format('u');
            $lottery_number = $referral_code + Participant::where('lottery_number', $referral_code)->count() + 1;
            $try -= 1;
        } while (Participant::where('lottery_number', $lottery_number)->exists());

        return $lottery_number;
    }
    static function random()
    {
        $count = Participant::whereNotNull('lottery_number')->max('lottery_number');
        if($count == 0) {
            $count = 1000;
        }
        return $count + 1;
    }
    public function func_get_dorprize_gift(){
        return $this->hasOne(Doorprize::class, 'participant_id', 'id');
    }
}
