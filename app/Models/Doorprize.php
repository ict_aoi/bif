<?php namespace App\Models;

use Config;
use App\Uuids;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doorprize extends Model
{
    use HasFactory,Uuids;
    // protected $table = 'doorprize';
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'];
    protected $fillable = [
        'participant_id',
        'doorprize_gift',
        'lottery_number',
        'nama',
        'description',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
