<?php

namespace App\Models;

use Config;
use App\Uuids;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListVote extends Model
{
    use HasFactory, Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'];
    protected $fillable = [
        'file_name',
        'thumb_name',
        'mime_content_type',
        'description',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = [
        'voter_image_url', 'video_url',
    ];

    public function getFullPath()
    {
        return Config::get('storage.asset') . '/' . e($this->filename);
    }

    public function getVideoUrlAttribute()
    {
        return $this->attributes['mime_content_type'] == 'video' ? route('backend.showVoterImage', $this->attributes['file_name']) : '-';
    }
    public function getVoterImageUrlAttribute()
    {
        return $this->attributes['mime_content_type'] == 'image' ? route('backend.showVoterImage', $this->attributes['file_name']) : route('backend.showVoterImage', $this->attributes['thumb_name']);
    }

    static function random($image)
    {
        $ret = [];
        $path = Config::get('storage.asset');
        $extension = $image->getClientOriginalExtension();
        $try = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(Str::random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;
            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value)
    {
        $pattern = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement = '-';
        $value = preg_replace($pattern, $replacement, $value);
        return $value;
    }
    public function get_votes(){
        
        return $this->hasMany(Vote::class, 'list_vote_id', 'id');
    }
}
