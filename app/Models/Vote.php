<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    use HasFactory,Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'];
    protected $fillable = [
        'list_vote_id',
        'participant_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
