<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RundownEvent extends Model
{
    use HasFactory,Uuids;

    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','event_date'];
    protected $fillable = [
        'event_date',
        'name',
        'description',
        'location',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
