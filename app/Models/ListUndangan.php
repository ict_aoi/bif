<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ListUndangan extends Model
{
    use HasFactory, Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $table        = 'list_undangan';
    protected $dates        = ['created_at'];
    protected $fillable = [
        'nik',
        'name',
    ];
}
