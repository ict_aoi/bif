<?php namespace App\Models\Middleware;

use Illuminate\Database\Eloquent\Model;

class Whatsapp extends Model
{
    protected $connection   = 'middleware_live';
    protected $table        = 'whatsapp';
    public $timestamps      = false;
    protected $fillable     = ['contact_number'
        ,'message'
        ,'is_group'
        ,'status'
        ,'description'
        ,'created_at'  
        ,'updated_at'  
        ,'url_attachment'
    ];
}
