<?php

namespace App\Console\Commands;

use App\Jobs\SendEmail;
use App\Jobs\SendNotification;
use App\Models\ListVote;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use stdClass;

class trial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trial';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $data = DB::select(db::raw("
        //     select * from (select list_votes.*,COALESCE(dtl.total,0) as total_vote
        //     from list_votes
        //     left join (
        //             select list_vote_id,count(0) total
        //             from votes
        //             GROUP BY list_vote_id
        //     ) dtl on dtl.list_vote_id = list_votes.id)tbl
        //     order by tbl.total_vote desc , tbl.id
        // "));
        // $array = array();
        // foreach ($data as $key => $value) {
        //     $obj        = new stdClass();
        //     $list_vote  = ListVote::find($value->id);
        //     $obj->id    = $list_vote->id;
        //     $obj->desc  = $list_vote->description;
        //     $obj->url   = $list_vote->voter_image_url;
        //     $obj->video_url = $list_vote->video_url;
        //     $obj->mime_content_type = $list_vote->mime_content_type;
        //     $obj->total = $value->total_vote;
        //     $array[]    = $obj;
        // }
        // dd($array);
        // return 0;
        $id = '62436e30-faac-11ed-a3dc-970a437078c6';
        SendNotification::dispatch($id);
    }
}
