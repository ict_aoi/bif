<?php

namespace App\Console\Commands;

use App\Jobs\SendNotification;
use App\Models\Participant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ResendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ResendEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $participants = Participant::whereNotNull('registration_at')->whereNotNull('email')->get();
        // foreach($participants as $participant){
        //     SendNotification::dispatch($participant);
        // }

        // SendNotification::dispatch($participants[0]);


        $data = $participants[0];

        $to = $data->email;
        $cc = [];

        // emails.invitation', ['data' => $this->data]
        Mail::send('emails.invitation', compact('data'), function ($message) use ($to,$cc)
            {
              $message->subject('BIF XIX - Together we rise');
              $message->from('aoi-noreply@bbi-apparel.com', 'aoi-noreply');
              $message->cc($cc);
              $message->to($to);
              // $message->attach($file, array('as' => $file_name.'.xlsx', 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
           });
    }
}
