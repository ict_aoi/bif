<?php namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class FormRundownEvent implements FromView,WithTitle
{
    public function title(): string
    {
        return 'active';
    }

    public function view(): View
    {
        return view('exports.form_rundown_event');
    }
}
