<?php

namespace App\Http\Controllers;

use App\Models\ListVote;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VoteController extends Controller
{
    public function foto()
    {
        // return 'tes';
        // return $list;
        
        return view('vote_foto');
    }
    public function video()
    {
        return view('vote_video');
    }
    public function dataLabels($kategori)
    {
        // $list = ListVote::orderBy('id')->pluck('description');
        $list = ListVote::orderBy('id')->where('mime_content_type', $kategori)->pluck('description');
        $collect = collect();
        foreach ($list as $l) {
            // $string = "Tiani (tia_nchan)";
            $character = substr($l, strpos($l, '(') + 1, strpos($l, ')') - strpos($l, '(') - 1);
            $character = strtoupper($character);
            $collect->push($character);
        }


        // return ['tes1', 'tes2', 'tes3', 'tes4', 'tes5'];
        return $collect;
    }
    public function getData($kategori)
    {
        $list = ListVote::where('mime_content_type', $kategori)->orderBy('id')->get();
        // return $list;
        $collect_vote = collect();
        foreach($list as $l){
            // return $l;
            $tamp = $l->get_votes->count();
            $collect_vote->push($tamp);
        }
        return $collect_vote;
        // $list = Listvote::orderBy('created_at')->pluck('id');
        
        // $vote = Vote::select(DB::raw('count(1) as total_votes'))
        //     ->groupBy('list_vote_id')
        //     ->orderBy('list_vote_id')
        //     ->pluck('total_votes');
        // // return [8, 9, 200, 80, 40, 50, 50];
        // return $vote;
    }
}
