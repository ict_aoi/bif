<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\ListUndangan;
use App\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class ScanBarcodeController extends Controller
{
    function safe_encode($string)
    {
        $data = str_replace(array('/'), array('_'), $string);
        return $data;
    }
    function safe_decode($string, $mode = null)
    {
        $data = str_replace(array('_'), array('/'), $string);
        return $data;
    }
    public function generateNIK($nik)
    {
        $barcode = $this->safe_encode(Crypt::encryptString($nik));
        return $barcode;
    }
    public function scan_checkin_($barcode)
    {
        // $dec_barcode = $this->safe_decode(Crypt::decryptString($barcode));
        $dec_barcode = $barcode;
        $participant = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->first();
        return ResponseFormatter::success(null, 'gagal');
        if (!isset($participant)) {
            return ResponseFormatter::success(null, 'gagal');
        }
        if($participant->peserta == 1){
            return ResponseFormatter::success($participant, 'peserta');
        }
        if ($participant->checkin == 1) {
            return ResponseFormatter::success($participant, 'scan');
        }
        // if($participant->peserta == 1){
        //     return ResponseFormatter::success($participant, 'gagal');

        // }

        try {

            DB::beginTransaction();
            $update = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->update([
                'checkin' => 1,
            ]);
           
            // $participant->checkin = 1;
            // $participant->save();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return ResponseFormatter::error('null', $th->getMessage());
        }
        return ResponseFormatter::success($participant, 'success');
    }
    public function scan_checkin($barcode)
    {
        // $dec_barcode = $this->safe_decode(Crypt::decryptString($barcode));
        $dec_barcode = $barcode;
        $participant = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->first();
        if (!isset($participant)) {
            return ResponseFormatter::success(null, 'gagal');
        }

        $undangan = ListUndangan::where('nik', $dec_barcode)->first();
        if (!isset($undangan)) {
            return ResponseFormatter::success(null, 'gagal');
        }

        // if($participant->peserta == 1){
        //     return ResponseFormatter::success($participant, 'peserta');
        // }
        if ($participant->checkin == 1) {
            return ResponseFormatter::success($participant, 'scan');
        }
        // if($participant->peserta == 1){
        //     return ResponseFormatter::success($participant, 'gagal');

        // }

        try {

            DB::beginTransaction();
            $update = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->update([
                'checkin' => 1,
                'food'    => 1,
                'photo'   => 1
            ]);
           
            // $participant->checkin = 1;
            // $participant->save();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return ResponseFormatter::error('null', $th->getMessage());
        }
        return ResponseFormatter::success($participant, 'success');
    }
    public function scan_food_($barcode)
    {
        // $dec_barcode = $this->safe_decode(Crypt::decryptString($barcode));
        $dec_barcode = $barcode;
        $participant = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->first();
        if (!isset($participant)) {
            return ResponseFormatter::success(null, 'gagal');
        }
        if($participant->peserta == 1){
            return ResponseFormatter::success($participant, 'peserta');
        }
        if ($participant->food == 1) {
            return ResponseFormatter::success($participant, 'scan');
        }

        try {

            DB::beginTransaction();
            
            $update = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->update([
                'food' => 1,
            ]);
            // $participant->checkin = 1;
            // $participant->save();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return ResponseFormatter::error('null', $th->getMessage());
        }
        return ResponseFormatter::success($participant, 'success');
    }
    public function scan_food($barcode)
    {
        // $dec_barcode = $this->safe_decode(Crypt::decryptString($barcode));
        $dec_barcode = $barcode;
        $participant = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->first();
        if (!isset($participant)) {
            return ResponseFormatter::success(null, 'gagal');
        }
        // if($participant->peserta == 1){
        //     return ResponseFormatter::success($participant, 'peserta');
        // }
        if ($participant->food == 1) {
            return ResponseFormatter::success($participant, 'scan');
        }

        try {

            DB::beginTransaction();
            
            $update = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->update([
                'food' => 1,
            ]);
            // $participant->checkin = 1;
            // $participant->save();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return ResponseFormatter::error('null', $th->getMessage());
        }
        return ResponseFormatter::success($participant, 'success');
    }
    // public function scan_photo($barcode)
    // {
    //     // $dec_barcode = $this->safe_decode(Crypt::decryptString($barcode));
    //     $dec_barcode = $barcode;
    //     $participant = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->first();
    //     if (!isset($participant)) {
    //         return ResponseFormatter::success(null, 'gagal');
    //     }
    //     if($participant->peserta == 1){
    //         return ResponseFormatter::success($participant, 'peserta');
    //     }
    //     if ($participant->photo == 1) {
    //         return ResponseFormatter::success($participant, 'scan');
    //     }

    //     try {

    //         DB::beginTransaction();
            
    //         $update = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->update([
    //             'photo' => 1,
    //         ]);
    //         // $participant->checkin = 1;
    //         // $participant->save();
    //         DB::commit();
    //     } catch (\Throwable $th) {
    //         DB::rollback();
    //         return ResponseFormatter::error('null', $th->getMessage());
    //     }
    //     return ResponseFormatter::success($participant, 'success');
    // }
    public function scan_photo($barcode)
    {
        // $dec_barcode = $this->safe_decode(Crypt::decryptString($barcode));
        $dec_barcode = $barcode;
        $participant = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->first();
        if (!isset($participant)) {
            return ResponseFormatter::success(null, 'gagal');
        }
        // if($participant->peserta == 1){
        //     return ResponseFormatter::success($participant, 'peserta');
        // }
        if ($participant->photo == 1) {
            return ResponseFormatter::success($participant, 'scan');
        }

        try {

            DB::beginTransaction();
            
            $update = Participant::where('nik', $dec_barcode)->whereNotNull('registration_at')->update([
                'photo' => 1,
            ]);
            // $participant->checkin = 1;
            // $participant->save();
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            return ResponseFormatter::error('null', $th->getMessage());
        }
        return ResponseFormatter::success($participant, 'success');
    }
}
