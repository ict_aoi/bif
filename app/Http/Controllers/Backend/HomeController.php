<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use File;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exports\FormParticipant as eFormParticipant;
use App\Imports\FormParticipant as iFormParticipant;
use App\Exports\FormRundownEvent as eFormRundownEvent;
use App\Imports\FormRundownEvent as iFormRundownEvent;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;

use App\Models\ListVote;
use App\Models\Participant;
use App\Models\RundownEvent;
use App\Notifications\TesMail;
use PDF;
use Spatie\Browsershot\Browsershot;
use Dompdf\Options;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{
    public function dataParticipants(Request $request)
    {
        $search_query   = $request->searchTerm;
        $perPage        = $request->per_page;
        // return $perPage;
        // return $search_query;
        // $participants   = Participant::select(
        //     'nik',
        //     'name',
        //     'birth_day',
        //     'lottery_number',
        //     'factory',
        //     'registration_at',
        //     'email',
        //     'phone_number',
        //     'created_at',
        //     'updated_at',
        //     'position',
        //     DB::raw("array_to_string(array_agg(circle), ', ') AS circle")
        // )->groupBy(
        //     'nik',
        //     'name',
        //     'birth_day',
        //     'lottery_number',
        //     'factory',
        //     'registration_at',
        //     'email',
        //     'phone_number',
        //     'created_at',
        //     'updated_at',
        //     'position',
        // )->where('name', 'LIKE', '%ghulam%')->latest()
        //     ->paginate($perPage)
        //     ->toArray();
        $participants = Participant::select(
            'nik',
            'name',
            'birth_day',
            'lottery_number',
            'factory',
            'registration_at',
            'email',
            'phone_number',
            'created_at',
            'updated_at',
            'position',
            DB::raw("array_to_string(array_agg(circle), ', ') AS circle")
        )
            ->groupBy(
                'nik',
                'name',
                'birth_day',
                'lottery_number',
                'factory',
                'registration_at',
                'email',
                'phone_number',
                'created_at',
                'updated_at',
                'position'
            )
            ->where(function ($query) use ($search_query) {
                $query->orwhere('name', 'ILIKE', "%$search_query%");
                $query->orwhere('nik', 'LIKE', "%$search_query%");
                $query->orwhere('factory', 'ILIKE', "%$search_query%");
                $query->orwhere('circle', 'ILIKE', "%$search_query%");
            })
            ->latest()
            ->paginate($perPage)
            ->toArray();


        // return $participants;

        // ->latest()
        //     ->paginate($perPage)
        //     ->toArray();
        // return $participants;
        if ($search_query) {
            $users['searchTerm'] = $search_query ?: '';
        } else {
            $users['searchTerm'] = $search_query ? null : '';
        }

        return ResponseFormatter::success([
            'participants'      => $participants,
        ], 'Success');
    }

    public function downloadFormParticipantExcel()
    {
        return Excel::download(new eFormParticipant, 'form_participants.xlsx');
    }

    public function storeParticipants(Request $request)
    {
        if ($request->selected_option == 'non_upload') {
            try {
                DB::beginTransaction();

                $validator = $request->validate([
                    'nik'       => 'required'
                ]);

                if ($request->_id == '-1') {
                    $is_exists = Participant::where(db::raw('lower(nik)'), strtolower($request->nik))->exists();
                    if (!$is_exists) {
                        $created_at = carbon::now()->todatetimestring();
                        $absen = DB::connection('middleware_live')
                            ->table('absen_all')->where('nik', $request->nik)->first();
                        if (!isset($absen)) {
                            return ResponseFormatter::error([
                                'nik'  => ['Nomor Induk Karyawan not found']
                            ], 'Insert failed', 422);
                        }
                        if ($request->integration_type == 'manual') {
                            $participant = Participant::Create([
                                'nik'           => $request->nik,
                                'name'          => $request->name,
                                'birth_day'     => $request->password,
                                'factory'       => $absen->factory,
                                'position'      => $absen->position_name,
                                'circle'        => $request->circle,
                                'created_at'    => $created_at,
                                'updated_at'    => $created_at,
                            ]);

                            DB::commit();

                            return ResponseFormatter::success([
                                'participant'      => $participant,
                            ], 'Success');
                        } else {
                            $absen = DB::connection('middleware_live')
                                ->table('absen_all')
                                ->where('nik', $request->nik)
                                ->first();
                            if (!isset($absen)) {
                                return ResponseFormatter::error([
                                    'nik'  => ['Nomor Induk Karyawan not found']
                                ], 'Insert failed', 422);
                            }
                            if ($absen) {
                                $participant = Participant::Create([
                                    'nik'           => $absen->nik,
                                    'name'          => $absen->name,
                                    'birth_day'     => $absen->absen_password,
                                    'factory'       => $absen->absen_factory,
                                    'circle'        => $absen->circle,
                                    'position'      => $absen->position_name,
                                    'created_at'    => $created_at,
                                    'updated_at'    => $created_at,
                                ]);

                                DB::commit();

                                return ResponseFormatter::success([
                                    'participant'      => $participant,
                                ], 'Success');
                            } else {
                                DB::rollBack();
                                return ResponseFormatter::error([
                                    'nik'  => ['Nomor Induk Karyawan not found']
                                ], 'Insert failed', 422);
                            }
                        }
                    } else {
                        DB::rollBack();
                        return ResponseFormatter::error([
                            'nik'  => ['Nik already exists']
                        ], 'Insert failed', 422);
                    }
                } else {
                    $participant = Participant::find($request->_id);
                    if ($participant) {
                        $updated_at                 = carbon::now()->todatetimestring();
                        $participant->nik           = $request->nik;
                        $participant->name          = $request->name;
                        $participant->birth_day     = $request->password;
                        $participant->updated_at    = $updated_at;
                        $participant->save();

                        DB::commit();

                        return ResponseFormatter::success([
                            'participant'      => $participant,
                        ], 'Success');
                    } else {
                        DB::rollBack();
                        return ResponseFormatter::error('Update failed', 422);
                    }
                }
            } catch (Exception $error) {

                DB::rollBack();
                return ResponseFormatter::error([
                    'nik'  => ['Nik is empty']
                ], 'Insert failed', 422);
            }
        } else {
            // return 'tesss';
            $request->validate([
                'upload_file' => 'required|file|mimes:xls,xlsx'
            ]);

            $import = new iFormParticipant();
            $import->onlySheets('active');


            Excel::import($import, $request->file('upload_file'));
            return ResponseFormatter::success('Import Success');
        }
    }

    public function sendMail_()
    {
        // return public_path();


        $data = Participant::where('nik', '11111111')
            ->whereNotNull('registration_at')
            ->first();
        // return view('emails.image', compact('data'));
        // $html = '<html><body><h1>Hello, World!</h1></body></html>';

        // $image = SnappyImage::loadHTML($html)->setOption('width', 800)->setOption('height', 600);

        // return $image->inline();
        $webRoot = public_path();

        // $pdf = new PDF($this->dompdf_options);

        $pdf = PDF::setBasePath($webRoot);
        $pdf->loadView('emails.image', ['data' => $data]);
        $pdf->setPaper('A4', 'portrait');
        $pdf->setOptions([
            'isPhpEnabled' => true,
            'isRemoteEnabled' => true,
            'isHtml5ParserEnabled' => true,
            'isFontSubsettingEnabled' => true,
            'defaultPaperSize' => 'A4',
            'defaultFont' => 'Helvetica',
            'dpi' => 150,
            'fontHeightRatio' => 0.8,
            'isFontHeightRatioFixed' => true,
            'margin_top' => 0,
            'margin_right' => 0,
            'margin_bottom' => 0,
            'margin_left' => 0,
            'enable_html5_parser' => true,
        ]);

        $pdf->save(public_path('images/sample.pdf'));
        return 'tesss';

        // return $pdf->stream();
    }
    public function sendMail()
    {
        $email = 'maulanacomara@gmail.com'; // Menggantinya dengan objek penerima notifikasi yang sesuai
        $message = "Ini adalah contoh pesan notifikasi";

        Notification::route('mail', $email)
            ->notify(new TesMail($message));
        return 'oke';
    }

    public function editParticipants(Request $request)
    {
        try {
            $is_exists = Participant::find($request->id);

            if ($is_exists) {
                return ResponseFormatter::success([
                    'participant' => $is_exists
                ], 'Edit Success');
            } else {
                return ResponseFormatter::error('Edit failed', 422);
            }
        } catch (Exception $error) {
            return ResponseFormatter::error('Edit failed', 422);
        }
    }

    public function deleteParticipants(Request $request)
    {
        try {

            $is_exists = Participant::find($request->id);
            if ($is_exists) {
                $is_exists->delete();
                return ResponseFormatter::success('Delete Success');
            } else {
                return ResponseFormatter::error('Delete failed', 422);
            }
        } catch (Exception $error) {
            return ResponseFormatter::error('Delete failed', 422);
        }
    }

    public function dataVoters(Request $request)
    {
        $search_query   = $request->searchTerm;
        $perPage        = $request->per_page;
        $listVoters     = ListVote::where('description', 'LIKE', '%' . $search_query . '%')
            ->latest()
            ->paginate($perPage)
            ->toArray();

        // return $listVoters['data'];

        // return $listVoters;

        if ($search_query) {
            $users['searchTerm'] = $search_query ?: '';
        } else {
            $users['searchTerm'] = $search_query ? null : '';
        }

        return ResponseFormatter::success([
            'records'      => $listVoters,
        ], 'Success');
    }

    public function showVoterImage($filename)
    {
        $path = Config::get('storage.asset');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function storeVoters(Request $request)
    {
        // return $request->all();

        try {
            $request->validate([
                'description' => 'required',
                // 'upload_file' => 'required',
                'checked_video' => 'required',
            ]);
            if ($request->checked_video == true && $request->checked_video != 'false') {
                // return 'tes';
                $request->validate([
                    'upload_file_thumb' => 'required|file',
                ]);
            }

            $storage = Config::get('storage.asset');
            if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);    
    
            $file       = $request->file('upload_file');
            $file_name  = ListVote::random($file)[1];
            $thumb_name  = '-';
            if ($request->checked_video == true && $request->checked_video != 'false') {
                $thumb      = $request->file('upload_file_thumb');
                $thumb_name  = ListVote::random($thumb)[1];
            }
            $file_type  = explode('/', $file->getClientMimeType())[0];
            // $thumb_type  = explode('/', $thumb->getClientMimeType())[0];
            $created_at = carbon::now()->todatetimestring();
    
            $list_vote = ListVote::Create([
                'description'       => $request->description,
                'file_name'         => $file_name,
                'thumb_name'         => $thumb_name,
                'mime_content_type' => $file_type,
                'created_at'        => $created_at,
                'updated_at'        => $created_at
            ]);
    
    
            if ($list_vote->save() && $file) {
                $file->move($storage, $file_name);
                if ($request->checked_video == true && $request->checked_video != 'false') {
                    $thumb->move($storage, $thumb_name);
                }
            }
        } catch (\Throwable $th) {
            return ResponseFormatter::error($th->getMessage());
        }

        return ResponseFormatter::success([
            'list_vote'      => $list_vote,
        ], 'Success');

        return ResponseFormatter::success('Insert Success');
    }

    public function deleteVoters(Request $request)
    {
        try {

            $is_exists = ListVote::find($request->id);
            if ($is_exists) {
                $image = $is_exists->getFullPath();

                if ($is_exists->delete() && File::exists($image))
                    @unlink($image);

                return ResponseFormatter::success('Delete Success');
            } else {
                return ResponseFormatter::error('Delete failed', 422);
            }
        } catch (Exception $error) {
            return ResponseFormatter::error('Delete failed', 422);
        }
    }

    public function dataRundownEvents(Request $request)
    {
        $search_query   = $request->searchTerm;
        $perPage        = $request->per_page;
        $records        = RundownEvent::where('description', 'LIKE', '%' . $search_query . '%')
            ->orWhere('name', 'LIKE', '%' . $search_query . '%')
            ->orWhere('location', 'LIKE', '%' . $search_query . '%')
            ->latest()
            ->paginate($perPage)
            ->toArray();

        if ($search_query) {
            $users['searchTerm'] = $search_query ?: '';
        } else {
            $users['searchTerm'] = $search_query ? null : '';
        }

        return ResponseFormatter::success([
            'records'      => $records,
        ], 'Success');
    }

    public function downloadFormRundownEventExcel()
    {
        return Excel::download(new eFormRundownEvent, 'form_rundown_event.xlsx');
    }

    public function editRundownEvents(Request $request)
    {
        try {
            $is_exists = RundownEvent::find($request->id);

            if ($is_exists) {
                return ResponseFormatter::success([
                    'record' => $is_exists
                ], 'Edit Success');
            } else {
                return ResponseFormatter::error('Edit failed', 422);
            }
        } catch (Exception $error) {
            return ResponseFormatter::error('Edit failed', 422);
        }
    }

    public function storeRundownEvents(Request $request)
    {
        if ($request->selected_option ==  'non_upload') {
            try {
                DB::beginTransaction();

                if ($request->_id == '-1') {
                    $created_at = carbon::now()->todatetimestring();

                    $rundown_event = RundownEvent::Create([
                        'event_date'    => $request->event_date,
                        'name'          => $request->name,
                        'description'   => $request->description,
                        'location'      => $request->location,
                        'created_at'    => $created_at,
                        'updated_at'    => $created_at,
                    ]);

                    DB::commit();

                    return ResponseFormatter::success([
                        'rundown_event'      => $rundown_event,
                    ], 'Success');
                } else {
                    $rundown_event = RundownEvent::find($request->_id);
                    if ($rundown_event) {
                        $updated_at                     = carbon::now()->todatetimestring();
                        $rundown_event->event_date      = $request->event_date;
                        $rundown_event->name            = $request->name;
                        $rundown_event->description     = $request->description;
                        $rundown_event->location        = $request->location;
                        $rundown_event->updated_at      = $updated_at;
                        $rundown_event->save();

                        DB::commit();

                        return ResponseFormatter::success([
                            'rundown_event'      => $rundown_event,
                        ], 'Success');
                    } else {
                        DB::rollBack();
                        return ResponseFormatter::error('Update failed', 422);
                    }
                }
            } catch (Exception $error) {

                DB::rollBack();
                return ResponseFormatter::error('Insert failed', 500);
            }
        } else {
            $request->validate([
                'upload_file' => 'required|file|mimes:xls,xlsx'
            ]);

            $import = new iFormRundownEvent();
            $import->onlySheets('active');


            Excel::import($import, $request->file('upload_file'));
            return ResponseFormatter::success('Import Success');
        }
    }

    public function deleteRundownEvents(Request $request)
    {
        try {

            $is_exists = RundownEvent::find($request->id);
            if ($is_exists) {
                $is_exists->delete();
                return ResponseFormatter::success('Delete Success');
            } else {
                return ResponseFormatter::error('Delete failed', 422);
            }
        } catch (Exception $error) {
            return ResponseFormatter::error('Delete failed', 422);
        }
    }
}
