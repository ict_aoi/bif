<?php

namespace App\Http\Controllers\Backend\Auth;

use Hash;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Models\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/backend/dashboard';

    public function index()
    {
        return view('auth.backend.index');
    }

    public function doLogin(Request $request)
    {
        // return 'tes';
        // return $request->all();
        try {
            $validator = $request->validate([
                'nik'       => 'required',
                'password'   => 'required'
            ]);

            $user = User::whereNull('deleted_at')
                ->whereNotNull('email_verified_at')
                ->where([
                    ['nik', $request->nik],
                    ['is_admin', true],
                ])
                ->first();
            // dd($user);
            if ($user) {
                if (Hash::check($request->password, $user->password)) {
                    /*Auth::guard('admins')->attempt([
                        'nik' => $request->nik,
                        'password' => $request->password,
                    ]);*/

                    return ResponseFormatter::success([
                        'token'      => $user->createToken('web based', ['role:admin'])->plainTextToken,
                    ], 'Success');
                } else {
                    return ResponseFormatter::error([
                        'password'  => ['password doesn\'t match']
                    ], 'Authentication failed', 500);
                }
            } else {
                return ResponseFormatter::error([
                    'nik'  => ['User is no exists']
                ], 'Authentication failed', 500);
            }
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'nik'  => ['User is no exists']
            ], 'Authentication failed', 500);
        }
    }

    public function doApiLogout(Request $request)
    {
        $user = request()->user();
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
        return ResponseFormatter::success([], 'Success');
    }
}
