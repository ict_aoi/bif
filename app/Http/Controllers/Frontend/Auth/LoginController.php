<?php

namespace App\Http\Controllers\Frontend\Auth;

use Hash;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;

use App\Models\User;


class LoginController extends Controller
{
    public function doLogin(Request $request)
    {
        try {
            $validator = $request->validate([
                'nik'       => 'required',
                'password'   => 'required'
            ]);
            // return $request->all();
            $user = User::whereNull('deleted_at')
                ->whereNotNull('email_verified_at')
                ->where([
                    ['nik', $request->nik],
                    ['is_admin', false],
                ])
                ->first();
            // dd($user, 'test');
            if ($user) {
                if (Hash::check($request->password, $user->password)) {
                    return ResponseFormatter::success([
                        'token'      => $user->createToken('web based', ['role:frontend'])->plainTextToken,
                    ], 'Success');
                } else {
                    return ResponseFormatter::error([
                        'password'  => ['password doesn\'t match']
                    ], 'Authentication failed', 500);
                }
            } else {
                return ResponseFormatter::error([
                    'nik'  => ['User is no exists']
                ], 'Authentication failed', 500);
            }
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'nik'  => ['User is no exists']
            ], 'Authentication failed', 500);
        }
    }
}
