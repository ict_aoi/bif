<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use PDF;
use Auth;
use File;
use Config;
use StdClass;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Http\Request;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Jobs\SendNotification;
use App\Notifications\SendMail;
use App\Models\Vote;
use App\Models\ListVote;
use App\Models\Doorprize;
use App\Models\Participant;
use App\Models\RundownEvent;

use App\Models\Middleware\Whatsapp;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\URL;
use Milon\Barcode\DNS2D;
use PhpParser\Node\Stmt\Return_;

class HomeController extends Controller
{
    function safe_encode($string)
    {
        $data = str_replace(array('/'), array('_'), $string);
        return $data;
    }
    function safe_decode($string, $mode = null)
    {
        $data = str_replace(array('_'), array('/'), $string);
        return $data;
    }
    public function table_barcode()
    {
        $participants = Participant::all();
        return view('pdf.barcode', compact('participants'));
    }

    public function barcode_data()
    {
        $data = Participant::query();

        return datatables()->of($data)

            ->addColumn('barcode', function ($data) {
                $html = '';
                $html .= '<img class="gambar_barcode" src="' . DNS2D::getBarcodePNGPath($data->nik, 'QRCODE', 10, 10) . '"
            alt="Barcode">';
                return $html;
            })
            ->addColumn('send_mail', function ($data) {
                $html = '';
                $html .= '<a onclick="sendMail(`' . route('barcode.sendMail', ['id' => $data->id]) . '`)" href="#!" class="btn btn-primary">Send</a>';
                return $html;
            })
            ->addColumn('is_send', function ($data) {
                if (isset($data->registration_at)) {
                    $html = '<span class="">' . $data->registration_at . ' </span>';
                } else {
                    $html = '<span class="">Not Send </span>';
                }
                return $html;
            })
            ->addColumn('card', function ($data) {
                return '<img src="' . URL::to('/') . $data->position . '" style="max-width:200px">';
            })
            ->addColumn('download', function ($data) {
                $html = '<a download="' . $data->name . '.png" href="' . URL::to('/') . $data->position . '" title="ImageName" class="btn btn-primary"> Download </a>';

                // $html = '<a download="'.URL::to('/').$data->position.'" class="btn btn-primary">Download</a>';
                return $html;
            })
            ->addIndexColumn()
            ->rawColumns(['barcode', 'send_mail', 'is_send', 'card', 'download'])
            ->make(true);
    }
    public function barcodesendMail($id)
    {
        try {
            DB::beginTransaction();
            $participant = Participant::where('id', $id)->first();
            // return $participant;
            // dd($participant);
            // SendNotification::dispatch($participant);
            $this->SendMail($participant);
            $participant->registration_at = now();
            $participant->save();
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'success'
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json($th->getMessage(), 422);
        }


        // return 
        // dd($id);
    }
    public function barcodesendAll()
    {
        try {
            $participants = Participant::whereNotNull('email')->get();
            foreach ($participants as $participant) {
                $participant->registration_at = now();
                // SendNotification::dispatch($participant);
                $this->SendMail($participant);
                $participant->save();
            }
            // return $participant;
            // dd($participant);
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'success'
            ]);
        } catch (\Throwable $th) {
            return response()->json($th->getMessage(), 422);
        }
    }
    public function registration(Request $request)
    {
        // return $request->all();
        $participant = Participant::where([
            'nik' => $request->nik
            // 'peserta' => 1
        ])->first();

        $current_date = carbon::now()->todatetimestring();
        if (isset($participant)) {
            return ResponseFormatter::error('Nik sudah terdaftar!', 422);
        }
        // if ($participant->birth_day != $request->password) {
        //     // return $request->password;
        //     return ResponseFormatter::error('Password not match', 422);
        // }
        // if ($current_date < '2022-07-03 00:00:01') {
        //     return ResponseFormatter::error('Registration is not open yet', 422);
        // }
        // if ($current_date > '2024-07-10 16:00:00') {
        //     return ResponseFormatter::error('Registration already closed', 422);
        // }
        try {
            $participant = Participant::where('nik', $request->nik)->first();
            $absen = DB::connection('middleware_live')->table('absen_all')->where('nik', $request->nik)->first();
            if (!isset($participant)) {
                // if (time() > strtotime('2023-07-08 06:30:00')) {
                $participant = Participant::Create([
                    'nik'           => $absen->nik,
                    'name'          => $absen->name,
                    'factory'       => $absen->factory,
                    'circle'        => 'UMUM',
                    'position'      => $absen->position,
                    'birth_day'     => (isset($absen->absen_password) && $absen->absen_password != null) ? $absen->absen_password : 000000,
                    'peserta'       => 0,
                    'lottery_number' => null,
                    'created_at'    => $current_date,
                    'updated_at'    => $current_date,
                ]);
                // }
            }

            DB::beginTransaction();
            // Vote::create([
            //     'list_vote_id'      => $request->vote_photo,
            //     'participant_id'    => $participant->id,
            //     'created_at'        => $participant->registration_at,
            //     'updated_at'        => $participant->registration_at,
            // ]);
            // $name           = ucwords($participant->name);
            // $lottery_number = $participant->lottery_number;

            // for (;;) {
            //     $lottery_number                 = Participant::random();
            //     $cek = Participant::where('lottery_number', $lottery_number)->first();
            //     if (!isset($cek)) {
            //         break;
            //     }
            // }
            $lottery_number                 = Participant::random();
            // $cek = Participant::where('lottery_number', $lottery_number)->first();
            // $data = Participant::where([
            //     ['nik', $participant->nik]
            // ])->update([
            //     'phone_number' => $request->phone_number,
            //     'email' => $request->email,
            //     'lottery_number' => $lottery_number,
            //     'registration_at' => $current_date,
            // ]);
            $participant->nik           = $absen->nik;
            $participant->name          = $absen->name;
            $participant->factory       = $absen->factory;
            $participant->circle        = 'UMUM';
            $participant->position      = $absen->position;
            $participant->birth_day     = (isset($absen->absen_password) && $absen->absen_password != null) ? $absen->absen_password : 000000;
            $participant->peserta       = 0;
            $participant->phone_number = $request->phone_number;
            $participant->email = $request->email;
            $participant->lottery_number = $lottery_number;
            $participant->registration_at = $current_date;

            $participant->save();

            // Vote::create([
            //     'list_vote_id'      => $request->vote_video,
            //     'participant_id'    => $participant->id,
            //     'created_at'        => $participant->registration_at,
            //     'updated_at'        => $participant->registration_at,
            // ]);
            $this->SendMail($participant);

            DB::commit();
            return ResponseFormatter::success([
                'participant'      => $participant,
            ], 'Success');
        } catch (\Throwable $th) {
            DB::rollBack();
            return ResponseFormatter::error($th->getMessage(), 422);
            // return $th->getMessage();
        }

        // try {
        //     DB::beginTransaction();
        //     $participant = Participant::where([
        //         ['nik', $request->nik],
        //     ])->first();
        //     if ($participant) {
        //         if (!$participant->registration_at) {
        //             if ($current_date >= '2022-09-10 00:00:01') {
        //                 if ($current_date <= '2023-09-10 23:59:59') {

        //                     //if($participant->birth_day == $request->password)
        //                     //{

        //                     $lottery_number                 = Participant::random();
        //                     //$participant->email             = $request->email;
        //                     $data = Participant::where([
        //                         ['nik', $participant->nik]
        //                     ])->update([
        //                         'phone_number' => $request->phone_number,
        //                         'email' => $request->email,
        //                         'lottery_number' => $lottery_number,
        //                         'registration_at' => $current_date,
        //                     ]);
        //                     // $participant->phone_number      = $request->phone_number;
        //                     // $participant->email             = $request->email;
        //                     // $participant->lottery_number    = $lottery_number;
        //                     // $participant->registration_at   = $current_date;
        //                     // $participant->save();

        //                     Vote::FirstOrCreate([
        //                         'list_vote_id'      => $request->vote,
        //                         'participant_id'    => $participant->id,
        //                         'created_at'        => $participant->registration_at,
        //                         'updated_at'        => $participant->registration_at,
        //                     ]);
        //                     $name           = ucwords($participant->name);
        //                     $lottery_number = $participant->lottery_number;
        //                     // $enc_nik = $this->safe_encode(Crypt::encryptString($participant->nik));
        //                     // $urlLink        = route('invitationCard', $enc_nik);

        //                     // $message = "*BIF16*|Dear $name,|Selamat Registrasi BIF16 telah berhasil,|berikut ini nomor undian anda *$lottery_number*|info lebih lanjut bisa klik link di bawah ini|$urlLink||BIF XVI akan dilaksanakan Sabtu, 11 September 2021 via live stream https://bit.ly/BIFXVI2021|Jangan lupa untuk subscribe channel Binabusana Group dan nyalakan lonceng notifikasi.|Pantengin terus acaranya dan jangan lupa comment semenarik dan sebanyak mungkin karena akan ada doorprize menarik|Terima kasih";

        //                     // Whatsapp::Create([
        //                     //     'contact_number'    => $request->phone_number,
        //                     //     'message'           => $message,
        //                     //     'is_group'          => false,
        //                     // ]);
        //                     $this->SendMail($participant);
        //                     // if ($participant->save()) {
        //                     //     // return $participant;
        //                     // }

        //                     DB::commit();
        //                     return ResponseFormatter::success([
        //                         'participant'      => $participant,
        //                     ], 'Success');

        //                     //}else  return ResponseFormatter::error('Password is wrong',422);
        //                 }
        //                 return ResponseFormatter::error('Registration already closed', 422);
        //             } else return ResponseFormatter::error('Registration is not open yet', 422);
        //         } else {
        //             return ResponseFormatter::error('Nik ' . $participant->nik . ' already register at ' . $participant->registration_at, 422);
        //         }
        //     } else return ResponseFormatter::error('Nik not found', 422);
        // } catch (\Throwable $th) {
        //     DB::rollBack();
        //     return $th->getMessage();
        // }
    }

    public function SendMail($participant)
    {
        // $email = $participant->email; // Menggantinya dengan objek penerima notifikasi yang sesuai
        // $message = "Ini adalah contoh pesan notifikasi";

        // Notification::route('mail', $email)
        //     ->notify(new SendMail($participant));
        // dd('halo');
        // SendNotification::dispatch($participant);

        $data = $participant;

        $to = $data->email;
        $cc = [];

        // emails.invitation', ['data' => $this->data]
        Mail::send('emails.invitation', compact('data'), function ($message) use ($to,$cc)
            {
              $message->subject('BIF XIX - Together we rise');
              $message->from('aoi-noreply@bbi-apparel.com', 'aoi-noreply');
              $message->cc($cc);
              $message->to($to);
              // $message->attach($file, array('as' => $file_name.'.xlsx', 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
           });

        // $participant;
    }

    public function logo()
    {
        $resp = response()->download(public_path('images/logo.png'));
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function rundownEvent(Request $request)
    {
        $event_dates = RundownEvent::select(db::raw('date(event_date) as event_date'))
            ->groupby(db::raw('date(event_date)'))
            ->get();

        $array = array();
        foreach ($event_dates as $key => $value) {
            $obj = new stdClass();
            $obj->event_date = $value->event_date->format('d/M/Y');

            $items = array();
            $event_items = RundownEvent::where(db::raw('date(event_date)'), $value->event_date)
                ->orderby('event_date', 'asc')
                ->get();

            foreach ($event_items as $key => $item) {
                $line               = new stdClass();
                $line->id           = ($key + 1);
                // $line->name         = ucfirst($item->name);
                $line->name         = $item->name;
                $line->time         = $item->event_date->format('h:i:s');
                // $line->description  = ucfirst(strtolower($item->description));
                // $line->location     = ucfirst(strtolower($item->location));
                $line->description  = $item->description;
                $line->location     = $item->location;
                $items[]           = $line;
            }
            $obj->items = $items;
            $array[] = $obj;
        }
        return ResponseFormatter::success([
            'rundown_event'      => $array,
        ], 'Success');
    }

    public function registeredParticipant(Request $request)
    {
        $participant = Participant::inRandomOrder()
            ->whereNotNull('registration_at')
            // ->where('is_exclude', false)
            ->whereNotIn('id', function ($query) {
                $query->select('participant_id')
                    ->from('doorprizes')
                    ->whereNotNull('participant_id');
            })
            ->get();

        return ResponseFormatter::success([
            'participant'      => $participant,
        ], 'Success');
    }

    public function candidatePhoto(Request $request)
    {
        $candidates = ListVote::inRandomOrder()
            ->take(15)
            ->where('mime_content_type', 'image')
            ->get();

        return ResponseFormatter::success([
            'candidates'      => $candidates,
        ], 'Success');
    }
    public function candidateVideo(Request $request)
    {
        $candidates = ListVote::inRandomOrder()
            ->take(15)
            ->where('mime_content_type', 'video')
            ->get();

        return ResponseFormatter::success([
            'candidates'      => $candidates,
        ], 'Success');
    }

    public function invitationCard($enc_nik)
    {
        // $nik = $this->safe_decode(Crypt::decryptString($enc_nik));
        $nik = $enc_nik;
        $data = Participant::where('nik', $nik)
            ->whereNotNull('registration_at')
            ->first();

        if ($data) return view('pdf.invitation', compact('data', 'enc_nik'));
        else return view('errors.404');
    }

    public function doorprize(Request $request)
    {
        $doorprize              = trim(strtolower($request->doorprizeTitle));
        $selectedParticipant    = $request->selectedParticipant;
        $created_at             = Carbon::now()->todatetimestring();

        foreach ($selectedParticipant as $key => $value) {
            if ($value) {
                $is_exists = Doorprize::where('participant_id', $value)->exists();

                if (!$is_exists) {
                    Doorprize::create([
                        'participant_id' => $value,
                        'doorprize_gift' => $doorprize,
                        'created_at'     => $created_at,
                        'updated_at'     => $created_at,
                    ]);
                }
            }
        }

        return ResponseFormatter::success('Success');
    }

    // UNDIAN PARTICIPANT CHECKIN - SLOT RODA
    public function dorprize_number()
    {
       // dd('peserta');
       $checkin = request()->checkin;
       // dd($peserta, 'tes');
       $list_jabatan = [
           'department head',
           'division head',
           'chief marketing officer',
           'chief finance officer',
           'chief supply chain officer',
           'chief executive officer',
           'chief operation officer',
           'sub division head',
           'coo',
           'cmo',
           'cfo',
           'csco',
           'ceo',
           'advisor',
           'factory manager',
       ];
       $participant = Participant::whereNotNull('registration_at')
           ->whereNotNull(['email', 'phone_number', 'lottery_number'])
           ->where('peserta', 1)
           ->where('checkin', $checkin)
           ->doesntHave('func_get_dorprize_gift')
           ->whereNotIn(DB::raw('lower(position)'), $list_jabatan)
           ->pluck('lottery_number')
           ->toArray();
       if (count($participant) <= 0) {
           return response()->json([
               'success' => false
           ]);
       }
       // return  $participant[array_rand($participant)];
       return response()->json([
           'success' => true,
           'number' => $participant[array_rand($participant)]
       ]);
    }

   // UNDIAN PARTICIPANT CHECKIN MOTOR - SLOT RODA
   public function dorprize_motor()
   {
       // dd('motor');
       $checkin = request()->checkin;
       // dd($peserta, 'tes');
       $list_jabatan = [
           'department head',
           'sub department head',
           'division head',
           'sub division head',
           'chief marketing officer',
           'chief finance officer',
           'chief supply chain officer',
           'chief executive officer',
           'chief operation officer',
           'customer service',
           'coo',
           'cmo',
           'cfo',
           'csco',
           'ceo',
           'advisor',
           'factory manager',
       ];
       $participant = Participant::whereNotNull('registration_at')
           ->whereNotNull(['email', 'phone_number', 'lottery_number'])
           ->where('peserta', 1)
           ->where('checkin', $checkin)
           ->where('factory', '<>', 'BBI')
           ->where(DB::raw("CAST(SUBSTRING(nik FROM 1 FOR 2) AS INTEGER)"), '<=', 22)
           ->doesntHave('func_get_dorprize_gift')
           ->whereNotIn(DB::raw('lower(position)'), $list_jabatan)
           ->pluck('lottery_number')
           ->toArray();
       if (count($participant) <= 0) {
           return response()->json([
               'success' => false
           ]);
       }
       // return  $participant[array_rand($participant)];
       return response()->json([
           'success' => true,
           'number' => $participant[array_rand($participant)]
       ]);
   }

    // UNDIAN PARTICIPANT NON CHECKIN - SLOT RODA
    public function dorprize_nocheckin()
    {
       
       $checkin = request()->checkin;
    //    dd($checkin, 'tes');
       $list_jabatan = [
           'department head',
           'division head',
           'chief marketing officer',
           'chief finance officer',
           'chief supply chain officer',
           'chief executive officer',
           'chief operation officer',
           'sub division head',
           'coo',
           'cmo',
           'cfo',
           'csco',
           'ceo',
           'advisor',
           'factory manager',
       ];
       $participant = Participant::whereNotNull('registration_at')
           ->whereNotNull(['email', 'phone_number', 'lottery_number'])
           ->where('peserta', 0)
        //    ->where('checkin', $checkin)
           ->where(DB::raw('lower(position)'), 'operator')
           ->doesntHave('func_get_dorprize_gift')
           ->where(DB::raw("CAST(SUBSTRING(nik FROM 1 FOR 2) AS INTEGER)"), '<=', 23)
           ->whereNotIn(DB::raw('lower(position)'), $list_jabatan)
           ->pluck('lottery_number')
           ->toArray();
       if (count($participant) <= 0) {
           return response()->json([
               'success' => false
           ]);
       }
       // return  $participant[array_rand($participant)];
       return response()->json([
           'success' => true,
           'number' => $participant[array_rand($participant)]
       ]);
    }

   // MENYIMPAN DATA PARTICIPANT YANG MENANG - SLOT RODA
   public function gift(Request $req)
   {
       try {
           DB::beginTransaction();
           $participant = Participant::where('lottery_number', implode($req->number))->get();
           foreach ($participant as $p) {
               Doorprize::create([
                   'participant_id' => $p->id,
                   'doorprize_gift' => isset($req->prize) ? $req->prize : '-',
                   'lottery_number' => $p->lottery_number,
                   'nama' => $p->name,
                   'description' => '-',
                   'created_at' => now(),
                   'updated_at' => now(),
               ]);
           }
           DB::commit();
           return 'success';
       } catch (\Throwable $e) {
           DB::rollBack();
           return $e->getMessage();
       }
   }

   // UNDIAN ALL PARTICIPANT - MULTI UNDIAN
   public function dorprize_number_gabungan()
   {
       // return request()->all();
    //    dd('multi undian peserta');
       $peserta = request()->peserta;
       $idx = request()->jumlah;
       $list_jabatan = [
           'department head',
           'division head',
           'chief marketing officer',
           'chief finance officer',
           'chief supply chain officer',
           'chief executive officer',
           'chief operation officer',
           'sub division head',
           'coo',
           'cmo',
           'cfo',
           'csco',
           'ceo',
           'advisor',
           'factory manager',
       ];
       $participant = Participant::whereNotNull('registration_at')
           ->whereNotNull(['email', 'phone_number', 'lottery_number'])
           ->where('peserta', 1)
           ->doesntHave('func_get_dorprize_gift')
           ->whereNotIn(DB::raw('lower(position)'), $list_jabatan)
           ->pluck('lottery_number')
           ->toArray();
       // return count($participant);
       // dd($participant->count());

       if (count($participant) < $idx) {
           return response()->json([
               'success' => false
           ]);
       }

       $collect = collect();
       foreach (array_rand($participant, $idx) as $a) {
           $tamp = $participant[$a];
           $collect->push($tamp);
       }
       return response()->json([
           'success' => true,
           'number' => $collect
       ]);
   }

   // MENYIMPAN DATA PARTICIPAN YANG MENANG - MULTI UNDIAN
   public function gift_gabungan(Request $req)
   {
    //    dd('simpan data menang peserta');
       $pemenang = $req->pemenang;
       try {
           DB::beginTransaction();
           foreach ($req->gift as $key => $gift) {
               $participant = Participant::where('lottery_number', $pemenang[$key])->first();
               Doorprize::create([
                   'participant_id' => $participant->id,
                   'doorprize_gift' => isset($gift) ? $gift : '-',
                   'lottery_number' => $participant->lottery_number,
                   'nama' => $participant->name,
                   'description' => '-',
                   'created_at' => now(),
                   'updated_at' => now(),
               ]);
           }
           DB::commit();
           return response()->json([
               'success' => true,
               'message' => 'success',
           ]);
       } catch (\Throwable $e) {
           DB::rollBack();
           return response()->json([
               'success' => false,
               'message' => 'Gagal ' . $e->getMessage() . ' - ' . $e->getLine(),
           ]);
       }

       // return $req->gift[0];
   }

   // UNDIAN YOUTUBE - MULTI UNDIAN
   public function dorprize_number_youtube()
   {
           // return request()->all();
        //    dd('multi undian peserta');
           $peserta = request()->peserta;
           $idx = request()->jumlah;
           $list_jabatan = [
               'department head',
               'division head',
               'chief marketing officer',
               'chief finance officer',
               'chief supply chain officer',
               'chief executive officer',
               'chief operation officer',
               'sub division head',
               'coo',
               'cmo',
               'cfo',
               'csco',
               'ceo',
               'advisor',
               'factory manager',
           ];
           $participant = Participant::whereNotNull('registration_at')
               ->whereNotNull(['email', 'phone_number', 'lottery_number'])
               ->where('peserta', 0)
               ->doesntHave('func_get_dorprize_gift')
               ->whereNotIn(DB::raw('lower(position)'), $list_jabatan)
               ->pluck('lottery_number')
               ->toArray();
   
           if (count($participant) < $idx) {
               return response()->json([
                   'success' => false
               ]);
           }
   
           $collect = collect();
           foreach (array_rand($participant, $idx) as $a) {
               $tamp = $participant[$a];
               $collect->push($tamp);
           }
           return response()->json([
               'success' => true,
               'number' => $collect
           ]);
   }

   // MENYIMPAN DATA YOUTUBE YANG MENANG - MULTI UNDIAN
   public function dorprize_gift_youtube(Request $req)
   {
           $pemenang = $req->pemenang;
           try {
               DB::beginTransaction();
               foreach ($req->gift as $key => $gift) {
                   $participant = Participant::where('lottery_number', $pemenang[$key])->first();
                   Doorprize::create([
                       'participant_id' => $participant->id,
                       'doorprize_gift' => isset($gift) ? $gift : '-',
                       'lottery_number' => $participant->lottery_number,
                       'nama' => $participant->name,
                       'description' => '-',
                       'created_at' => now(),
                       'updated_at' => now(),
                   ]);
               }
               DB::commit();
               return response()->json([
                   'success' => true,
                   'message' => 'success',
               ]);
           } catch (\Throwable $e) {
               DB::rollBack();
               return response()->json([
                   'success' => false,
                   'message' => 'Gagal ' . $e->getMessage() . ' - ' . $e->getLine(),
               ]);
           }
   
           // return $req->gift[0];
   }


    public function voteChart(Request $request)
    {
        $data = DB::select(db::raw("
            select * from (select list_votes.*,COALESCE(dtl.total,0) as total_vote
            from list_votes
            left join (
                    select list_vote_id,count(0) total
                    from votes
                    GROUP BY list_vote_id
            ) dtl on dtl.list_vote_id = list_votes.id)tbl
            order by tbl.total_vote desc , tbl.id
        "));
        $array = array();
        foreach ($data as $key => $value) {
            $obj        = new stdClass();
            $list_vote  = ListVote::find($value->id);
            $obj->id    = $list_vote->id;
            $obj->desc  = $list_vote->description;
            $obj->url   = $list_vote->voter_image_url;
            $obj->video_url = $list_vote->video_url;
            $obj->mime_content_type = $list_vote->mime_content_type;
            $obj->total = $value->total_vote;
            $array[]    = $obj;
        }

        return ResponseFormatter::success([
            'chart' => $array
        ], 'Success');
    }
    public function checkVoting_old($nik, $pass)
    {
        $participant = Participant::where('nik', $nik)->first();
        $peserta = DB::connection('middleware_live')
            ->table('absen_all')->where('nik', $nik)->first();
        return ResponseFormatter::success(null, false);

        return ResponseFormatter::success([
            'message' => 'Registration already closed'
        ], true);
        if (isset($peserta) && !isset($participant) && time() < strtotime('2023-07-08 06:30:00')) {
            return ResponseFormatter::success([
                'message' => 'Registration not open yet'
            ], true);
        }

        if (!isset($peserta)) {
            return ResponseFormatter::success([
                'message' => 'Nomor Induk Karyawan not found'
            ], true);
        }

        // if (!isset($participant)) {
        //     return ResponseFormatter::success([
        //         'message' => 'NIK not Found / NIK not registered in participant'
        //     ], true);
        // }
        if ($pass != $peserta->absen_password && $pass != $peserta->hris_password) {
            return ResponseFormatter::success([
                'message' => 'Password Not Match'
            ], true);
        }


        if ((isset($participant) && $participant->registration_at == null && $participant->lottery_number == null) || (isset($peserta) && !isset($participant))) {
            return ResponseFormatter::success(null, false);
        }
        // if(i)
        // return ResponseFormatter::success(null, false);
        return ResponseFormatter::success([
            'message' => 200,
            'participant'      => $participant,
        ], true);
    }

    public function checkVoting($nik)
    {
        try {
            $participant = Participant::where('nik', $nik)->first();
            $peserta = DB::connection('middleware_live')
                ->table('absen_all')->where('nik', $nik)->first();
            // return ResponseFormatter::success(null, false);

            // return ResponseFormatter::success([
            //     'message' => 'Registration already closed'
            // ], true);

            // if (isset($peserta) && !isset($participant) && time() < strtotime('2023-07-08 06:30:00')) {
            //     return ResponseFormatter::success([
            //         'message' => 'Registration not open yet'
            //     ], true);
            // }

            if (!isset($peserta)) {
                return ResponseFormatter::success([
                    'message' => 'Nomor Induk Karyawan not found'
                ], true);
            }


            if ((isset($participant) && $participant->registration_at == null && $participant->lottery_number == null) || (isset($peserta) && !isset($participant))) {
                return ResponseFormatter::success(null, false);
            }
            // if(i)
            // return ResponseFormatter::success(null, false);
            return ResponseFormatter::success([
                'message' => 200,
                'participant'      => $participant,
            ], true);
        } catch (\Throwable $th) {
            return ResponseFormatter::error([
                'message' => $th->getMessage()
            ], true);
        }
    }

    public function doorprize_gabungan($idx)
    {
        return view('doorprize_gabungan', compact('idx'));
    }

    public function doorprize_youtube($idx)
    {
        return view('doorprize_youtube', compact('idx'));
    }
}
