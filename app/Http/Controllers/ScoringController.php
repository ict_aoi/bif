<?php

namespace App\Http\Controllers;

use App\Models\Score;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScoringController extends Controller
{
    private function team(){
        return Score::orderBy('team')->limit(4)->get();
    }
    public function scoring()
    {
        $score = $this->team();
        return view('scoring', compact('score'));
    }
    public function scoring_dashboard()
    {
        $score = $this->team();
        return view('scoring_dashboard', compact('score'));
    }
    public function get()
    {
        return $this->team();
    }
    public function update()
    {
        // return request()->all();
        try {
            foreach (request()->teams as $key => $score) {
                $data = Score::where('id', $score['id'])->first();
                $data->score = $score['value'];
                $data->save();
            }
            return json_encode('successs');
        } catch (\Throwable $e) {
            return json_encode('gagal '.$e->getMessage());
        }
    }
}
