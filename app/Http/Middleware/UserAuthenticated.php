<?php namespace App\Http\Middleware;

use Closure;
use App\Helpers\ResponseFormatter;

class UserAuthenticated
{
   
    public function handle($request, Closure $next)
    {
        if (auth()->user()->tokenCan('role:frontend')) {
            return $next($request);
        }

        return ResponseFormatter::error('Not Authorized',401);
    }
}
