<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Models\Participant;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/checkvoting/{nik}/{password}', [App\Http\Controllers\Frontend\HomeController::class, 'checkVoting']);
Route::get('/checkvoting/{nik}', [App\Http\Controllers\Frontend\HomeController::class, 'checkVoting']);
// Route::get('/random_nomor', function () {
//     return 'tes';
// });
Route::get('/authenticated', function ()  // ini buat cek admin
{
    return auth()->user();
})
    ->middleware(['auth:sanctum', 'admin.auth']);

Route::get('/user-authenticated', function ()  // ini buat cek non admin
{
    return auth()->user();
})
    ->middleware(['auth:sanctum', 'user.auth']);

Route::post('/login', [App\Http\Controllers\Frontend\Auth\LoginController::class, 'doLogin'])->name('doLogin');
Route::get('/registered-participant', [App\Http\Controllers\Frontend\HomeController::class, 'registeredParticipant'])->middleware(['auth:sanctum','user.auth']);


Route::get('/dorpirze/number', [App\Http\Controllers\Frontend\HomeController::class, 'dorprize_number'])->name('dorprize_number');
Route::get('/dorpirze/motor', [App\Http\Controllers\Frontend\HomeController::class, 'dorprize_motor'])->name('dorprize_motor');
Route::get('/dorpirze/nocheckin', [App\Http\Controllers\Frontend\HomeController::class, 'dorprize_nocheckin'])->name('dorprize_nocheckin');
Route::get('/dorpirze/gift', [App\Http\Controllers\Frontend\HomeController::class, 'gift'])->name('dorprize_gift');


Route::get('/dorpirze/number_gabungan', [App\Http\Controllers\Frontend\HomeController::class, 'dorprize_number_gabungan'])->name('dorprize_number_gabungan');
Route::get('/dorpirze/gift_gabungan', [App\Http\Controllers\Frontend\HomeController::class, 'gift_gabungan'])->name('dorprize_gift_gabungan');


Route::get('/dorpirze/youtube', [App\Http\Controllers\Frontend\HomeController::class, 'dorprize_number_youtube'])->name('dorprize_number_youtube');
Route::get('/dorpirze/gift_youtube', [App\Http\Controllers\Frontend\HomeController::class, 'dorprize_gift_youtube'])->name('dorprize_gift_youtube');


Route::get('/rundown-event', [App\Http\Controllers\Frontend\HomeController::class, 'rundownEvent']);
Route::get('/candidate_photo', [App\Http\Controllers\Frontend\HomeController::class, 'candidatePhoto']);
Route::get('/candidate_video', [App\Http\Controllers\Frontend\HomeController::class, 'candidateVideo']);
Route::get('/invitation-card/{id}', [App\Http\Controllers\Frontend\HomeController::class, 'invitationCard'])->name('invitationCard');
Route::get('/vote-chart', [App\Http\Controllers\Frontend\HomeController::class, 'voteChart'])->middleware(['auth:sanctum', 'user.auth']);
Route::post('/doorprize', [App\Http\Controllers\Frontend\HomeController::class, 'doorprize'])->middleware(['auth:sanctum', 'user.auth']);
Route::post('/registration', [App\Http\Controllers\Frontend\HomeController::class, 'registration']);

Route::get('/checkin/{barcode}', [App\Http\Controllers\Backend\ScanBarcodeController::class, 'scan_checkin'])->middleware(['auth:sanctum', 'admin.auth']);
Route::get('/food/{barcode}', [App\Http\Controllers\Backend\ScanBarcodeController::class, 'scan_food'])->middleware(['auth:sanctum', 'admin.auth']);
Route::get('/photo/{barcode}', [App\Http\Controllers\Backend\ScanBarcodeController::class, 'scan_photo'])->middleware(['auth:sanctum', 'admin.auth']);
Route::get('/generate/{nik}', [App\Http\Controllers\Backend\ScanBarcodeController::class, 'generateNIK'])->middleware(['auth:sanctum', 'admin.auth']);

Route::get('/data-participants', [App\Http\Controllers\Backend\HomeController::class, 'dataParticipants'])->name('backend.dataParticipants')->middleware(['auth:sanctum', 'admin.auth']);
Route::get('/edit-participants', [App\Http\Controllers\Backend\HomeController::class, 'editParticipants'])->name('backend.editParticipants')->middleware(['auth:sanctum', 'admin.auth']);
Route::post('/invitation-card', [App\Http\Controllers\Backend\HomeController::class, 'invitationCard'])->name('backend.invitationCard')->middleware(['auth:sanctum', 'admin.auth']);
Route::get('/download-form-participants', [App\Http\Controllers\Backend\HomeController::class, 'downloadFormParticipantExcel']);
Route::post('/store-participants', [App\Http\Controllers\Backend\HomeController::class, 'storeParticipants'])->middleware(['auth:sanctum', 'admin.auth']);
Route::delete('/delete-participants', [App\Http\Controllers\Backend\HomeController::class, 'deleteParticipants'])->name('backend.deleteParticipants')->middleware(['auth:sanctum', 'admin.auth']);

Route::get('/data-voters', [App\Http\Controllers\Backend\HomeController::class, 'dataVoters'])->name('backend.dataVoters')->middleware(['auth:sanctum', 'admin.auth']);
Route::get('/show-voter-images/{filename}', [App\Http\Controllers\Backend\HomeController::class, 'showVoterImage'])->name('backend.showVoterImage');
Route::post('/store-voters', [App\Http\Controllers\Backend\HomeController::class, 'storeVoters'])->name('backend.storeVoters')->middleware(['auth:sanctum', 'admin.auth']);
Route::delete('/delete-voters', [App\Http\Controllers\Backend\HomeController::class, 'deleteVoters'])->name('backend.deleteVoters')->middleware(['auth:sanctum', 'admin.auth']);

Route::get('/data-rundown-events', [App\Http\Controllers\Backend\HomeController::class, 'dataRundownEvents'])->name('backend.dataRundownEvents')->middleware(['auth:sanctum', 'admin.auth']);
Route::get('/download-form-rundown-events', [App\Http\Controllers\Backend\HomeController::class, 'downloadFormRundownEventExcel']);
Route::get('/edit-rundown-events', [App\Http\Controllers\Backend\HomeController::class, 'editRundownEvents'])->name('backend.editRundownEvents')->middleware(['auth:sanctum', 'admin.auth']);
Route::post('/store-rundown-events', [App\Http\Controllers\Backend\HomeController::class, 'storeRundownEvents'])->name('backend.storeRundownEvents')->middleware(['auth:sanctum', 'admin.auth']);
Route::delete('/delete-rundown-events', [App\Http\Controllers\Backend\HomeController::class, 'deleteRundownEvents'])->name('backend.deleteRundownEvents')->middleware(['auth:sanctum', 'admin.auth']);

Route::post('/logout', [App\Http\Controllers\Backend\Auth\LoginController::class, 'doApiLogout'])->middleware(['auth:sanctum', 'admin.auth']);

// Route::get('/tes/email', function () {
//     $data = Participant::where('nik', '11111111')
//         ->whereNotNull('registration_at')
//         ->first();
//     $path = Config::get('storage.asset');

//     Mail::to('maulanacomara@gmail.com')->send(new SendMail($data));

//     return 'tes';
// });
