<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\ScoringController;
use App\Http\Controllers\VoteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/barcode', [HomeController::class, 'table_barcode'])->name('table_barcode');
Route::get('/barcode/data', [HomeController::class, 'barcode_data'])->name('barcode.data');
Route::get('/hasil_vote_foto', [VoteController::class, 'foto'])->name('vote.foto');
Route::get('/hasil_vote_video', [VoteController::class, 'video'])->name('vote.video');
Route::get('/data_label/{kategori}', [VoteController::class, 'dataLabels'])->name('vote.dataLabels');
Route::get('/getData/{kategori}', [VoteController::class, 'getData'])->name('vote.getData');
Route::get('/scoring', [ScoringController::class, 'scoring'])->name('score.scoring');
Route::get('/scoring_dashboard', [ScoringController::class, 'scoring_dashboard'])->name('score.scoring_dashboard');
Route::get('/update/score', [ScoringController::class, 'update'])->name('score.update');
Route::get('/get/score', [ScoringController::class, 'get'])->name('score.get');
Route::get('email/tes', [App\Http\Controllers\Backend\HomeController::class, 'sendMail']);
Route::get('email/tes', [App\Http\Controllers\Backend\HomeController::class, 'sendMail']);

Route::get('/peserta/{idx}', [HomeController::class, 'doorprize_gabungan'])->name('doorprize_gabungan');
Route::get('/youtube/{idx}', [HomeController::class, 'doorprize_youtube'])->name('doorprize_youtube');

Route::get('/barcode/sendMail/{id}',[HomeController::class, 'barcodesendMail'])->name('barcode.sendMail');
Route::get('/barcode/sendAll',[HomeController::class, 'barcodesendAll'])->name('barcode.sendAll');
Route::get('/', function () {
    // return Redirect::to('https://bbi-apparel.com');
    return view('pages.frontend.index');
});
Route::get('/logo', [App\Http\Controllers\Frontend\HomeController::class, 'logo']);

Route::get('/csrf', function () {
    return csrf_field();
});
Route::group(['prefix' => 'backend', 'namespace' => 'Backend'], function () {
    Route::get('/', function () {
        return view('pages.backend.index');
    });

    Route::post('/backend-login', [App\Http\Controllers\Backend\Auth\LoginController::class, 'doLogin'])->name('backend.doLogin');

    Route::post('/backend-session-logout', [App\Http\Controllers\Backend\Auth\LoginController::class, 'doSessionLogout'])->name('backend.doSessionLogout');

    Route::any('/{any}', function () {
        return view('pages.backend.index');
    })
        ->where('any', '.*');
});

Route::any('{any}', function () {
    return view('pages.frontend.index');
})
    ->where('any', '.*');
