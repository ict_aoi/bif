const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .combine([
        'resources/js/admin/vendors.min.js',
        'resources/js/admin/LivIconsEvo/js/LivIconsEvo.tools.js',
        'resources/js/admin/LivIconsEvo/js/LivIconsEvo.defaults.js',
        'resources/js/admin/LivIconsEvo/js/LivIconsEvo.min.js',
        'resources/js/admin/vertical-menu-light.js',
        'resources/js/admin/app-menu.js',
        'resources/js/admin/app.js',
        'resources/js/admin/footer.js',
        'resources/js/admin/sweetalert2.all.min.js',
    ], 'public/js/templete_admin.js')
    .combine([
        'resources/js/frontend/modernizr.min.js',
        'resources/js/frontend/pace.js',
    ], 'public/js/frontend_header.js')
    .combine('resources/js/admin/sweetalert2.all.min.js', 'public/js/sweetalert2.js')
    .combine('resources/js/frontend/jquery.min.js', 'public/js/jquery.min.js')
    .combine('resources/js/frontend/bootstrap.min.js', 'public/js/bootstrap.min.js')
    .combine('resources/js/frontend/countdown.js', 'public/js/countdown.js')
    .combine('resources/js/frontend/wow.js', 'public/js/wow.js')
    .combine('resources/js/frontend/slick.js', 'public/js/slick.js')
    .combine('resources/js/frontend/magnific-popup.js', 'public/js/magnific-popup.js')
    .combine('resources/js/frontend/appear.js', 'public/js/appear.js')
    .combine('resources/js/frontend/count-to.js', 'public/js/count-to.js')
    .combine('resources/js/frontend/jquery.nicescroll.min.js', 'public/js/nicescroll.js')
    .combine('resources/js/frontend/main.js', 'public/js/main.js')
    .combine([
        'resources/css/admin/vendors.min.css',
        'resources/css/admin/swiper.min.css',
        'resources/css/admin/bootstrap.css',
        'resources/css/admin/bootstrap-extended.css',
        'resources/css/admin/colors.css',
        'resources/css/admin/components.css',
        'resources/css/admin/dark-layout.css',
        'resources/css/admin/semi-dark-layout.css',
        'resources/css/admin/vertical-menu.css',
        'resources/css/admin/sweetalert2.min.css',
        'resources/css/admin/style.css',
    ], 'public/css/templete_admin.css')
    .combine([
        'resources/css/frontend/bootstrap.min.css',
        'resources/css/frontend/animate.css',
        'resources/css/frontend/slick.css',
        'resources/css/frontend/magnific-popup.css',
        'resources/css/frontend/font-awesome.css',
        'resources/css/frontend/streamline-icons.css',
        'resources/css/frontend/streamline-icons.css',
        'resources/css/frontend/event.css',
        'resources/css/frontend/theme.css',
        'resources/css/admin/sweetalert2.min.css',
    ], 'public/css/landing_page.css')
    .combine([
        'resources/css/frontend/invitation_pdf.css',
    ], 'public/css/invitation_pdf.css')
    .copyDirectory('resources/data', 'public/data')
    .copyDirectory('resources/fonts', 'public/fonts')
    .copyDirectory('resources/images', 'public/images')
    .vue()
    .version();