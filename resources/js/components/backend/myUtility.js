import Swal from 'sweetalert';
import store from '../../store';
import router from '../../routes';




const headers = {
    'Content-Type': 'application/json',
    "Authorization": "Bearer " + store.state.token,
};

export function myalert(status, message){
    if(status == 'success'){
        Swal({
            icon: status,
            title: 'Good Job!',
            text: message,
            type: status,
            timer: 2000, // time in milliseconds
            // timerProgressBar: true,
            buttons:false,
            closeOnEsc: false,
            closeOnClickOutside: false,
            closeModal: false,
            onBeforeOpen: () => {
              Swal.showLoading()
            },
            onClose: () => {
              // do something after the pop-up is closed
            }
          }).then((result) => {
            document.getElementById("barcodeScan").focus();
            
          })
        // Swal('Good Job!', message, status);
    }else if(status == 'warning'){
        Swal({
            icon: status,
            title: 'Warning!',
            text: message,
            type: status,
            timer: 2000, // time in milliseconds
            // timerProgressBar: true,
            buttons:false,
            closeOnEsc: false,
            closeOnClickOutside: false,
            closeModal: false,
            onBeforeOpen: () => {
              Swal.showLoading()
            },
            onClose: () => {
              // do something after the pop-up is closed
            }
          }).then((result) => {
            document.getElementById("barcodeScan").focus();
          })
        // Swal('Warning!', message, status);

    }else{
        Swal({
            icon: status,
            title: 'Error!',
            text: message,
            type: status,
            timer: 2000, // time in milliseconds
            // timerProgressBar: true,
            buttons:false,
            closeOnEsc: false,
            closeOnClickOutside: false,
            closeModal: false,
            onBeforeOpen: () => {
              Swal.showLoading()
            },
            onClose: () => {
              // do something after the pop-up is closed
            }
          }).then((result) => {
            document.getElementById("barcodeScan").focus();

          })
        // Swal('Error!', message, status);
    }
    
}

export function scanBarcode(barcode, stage){
    $('#no_data').removeClass('hidden');
    $('#data').addClass('hidden');
    let url = '/api';
    if(stage == 'checkin'){
        url = url+'/checkin';
    }else if(stage == 'food'){
        url = url+'/food';
        
    }else if(stage == 'photo'){
        url = url+'/photo';
    }
    url = url+'/'+barcode;
    console.log(url);
    axios.get('/api/authenticated', { headers: headers }).then((response) => {
        $('#barcodeScan').val('');
        store.commit('userAuthenticated', {
            isLogin: true,
            user: response.data
        });


        axios.get(url, {headers: headers}).then((response) => {
                $('#name').text('');
                $('#nik').text('');
                $('#circle').text('');
                $('#no_wa').text('');
                $('#status').text('');
            if(response.data.meta.message == 'gagal'){
                $('#no_data').removeClass('hidden');
                $('#data').addClass('hidden');
                return myalert('error', 'User not found!');
            }
            
            let data = response.data.data;
                $('#no_data').addClass('hidden');
                $('#data').removeClass('hidden');
                $('#name').text(data.name);
                $('#nik').text(data.nik);
                $('#circle').text(data.circle);
                $('#no_wa').text(data.email);
            if(response.data.meta.message == 'checkin'){
                $('#status').html('<i class="fa fa-times-circle fa-lg text-danger"></i>');
                return myalert('error', 'The User has not done a Check-In scan')
            }
            if(response.data.meta.message == 'peserta'){
                // $('#status').html('<i class="fa fa-times-circle fa-lg text-danger"></i>');
                return myalert('error', 'The User is not included in the invited guest');
            }
            if(response.data.meta.message == 'scan'){
                $('#status').html('<i class="fa fa-exclamation-triangle fa-lg text-warning"></i>');
                return myalert('warning', 'The User has been Scanned')
            }
            $('#status').html('<i class="fa fa-check-circle fa-lg text-success"></i>');
            return myalert('success', 'Check In Success');
            
            // console.log(response.data)
        }).catch(() => {
            return myalert('error', 'Scan Failure!!');
        });
        
    }).catch(() => {
        store.commit('userAuthenticated', {
            isLogin: false,
            user: {}
        });


        router.push({ name: 'login' });
    });
}