import login from './components/backend/login';
import LandingPage from './components/frontend/LandingPage';
import Votes from './components/frontend/Votes';
import Doorprize from './components/frontend/Doorprize';
import not_found from './components/backend/404';
import dashboard_backend from './components/backend/dashboard';
import dashboard_scanCheckIn from './components/backend/ScanCheckIn';
import dashboard_scanKonsumsi from './components/backend/ScanKonsumsi';
import dashboard_scanPhoto from './components/backend/ScanPhoto';
import store from './store';
import slot from './components/dorprize/SlotMachine.vue'
import motor from './components/dorprize/SlotMachineMotor.vue'
import nocheckin from './components/dorprize/SlotMachineNoCheckin.vue'

export default {
    mode: 'history',
    linkActiveClass: 'active',
    inkExactActiveClass: 'active',
    routes: [{
            path: '*',
            component: not_found
        },
        {
            path: '/',
            name: 'landing_page',
            component: LandingPage,

        },
        {
            path: '/votes',
            name: 'votes',
            component: Votes,
            beforeEnter: (to, form, next) => {
                store.commit('setFrontPage', {
                    selectedFrontPage: 'votes',
                });

                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.tokenFront,
                };

                axios.get('/api/user-authenticated', { headers: headers }).then(() => {
                    store.commit('frontAuthenticated', {
                        isUserLogin: true,
                        showLoginDialog: false,
                        tokenFront: store.state.tokenFront,
                    });

                    next();
                }).catch(() => {
                    store.commit('frontAuthenticated', {
                        isUserLogin: false,
                        showLoginDialog: true,
                        tokenFront: store.state.tokenFront,
                    });

                    next({ name: 'landing_page' });
                    //next(false)

                });
            }
        },
        {
            path: '/doorpize',
            name: 'doorpize',
            component: Doorprize,
            beforeEnter: (to, form, next) => {

                store.commit('setFrontPage', {
                    selectedFrontPage: 'doorpize',
                });


                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.tokenFront,
                };

                axios.get('/api/user-authenticated', { headers: headers }).then(() => {
                    store.commit('frontAuthenticated', {
                        isUserLogin: true,
                        showLoginDialog: false,
                        tokenFront: store.state.tokenFront,
                    });


                    next();
                }).catch(() => {
                    store.commit('frontAuthenticated', {
                        isUserLogin: false,
                        showLoginDialog: true,
                        tokenFront: store.state.tokenFront,
                    });

                    next({ name: 'landing_page' });
                    //next(false)

                });
            }
        },
        {
            path: '/backend',
            component: login,
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/authenticated', { headers: headers }).then(() => {
                    store.commit('userAuthenticated', {
                        isLogin: true,
                        user: response.data
                    });

                    next({ name: 'dashboard_backend' });
                }).catch(() => {
                    store.commit('userAuthenticated', {
                        isLogin: false,
                        user: {}
                    });

                    next({ name: 'login' });
                });
            }
        },
        {
            path: '/backend/login',
            component: login,
            name: 'login',
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/authenticated', { headers: headers }).then((response) => {
                    store.commit('userAuthenticated', {
                        isLogin: true,
                        user: response.data
                    });

                    next({ name: 'dashboard_backend' });
                }).catch(() => {
                    store.commit('userAuthenticated', {
                        isLogin: false,
                        user: {}
                    });

                    next();
                });
            }
        },
        {
            path: "/backend/dashboard",
            name: "dashboard_backend",
            component: dashboard_backend,
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/authenticated', { headers: headers }).then((response) => {
                    store.commit('userAuthenticated', {
                        isLogin: true,
                        user: response.data
                    });


                    next();
                }).catch(() => {
                    store.commit('userAuthenticated', {
                        isLogin: false,
                        user: {}
                    });


                    next({ name: 'login' });
                });
            }

        },
        {
            path: "/backend/scancheckin",
            name: "dashboard_scanCheckIn",
            component: dashboard_scanCheckIn,
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/authenticated', { headers: headers }).then((response) => {
                    store.commit('userAuthenticated', {
                        isLogin: true,
                        user: response.data
                    });


                    next();
                }).catch(() => {
                    store.commit('userAuthenticated', {
                        isLogin: false,
                        user: {}
                    });


                    next({ name: 'login' });
                });
            }

        },
        {
            path: "/backend/scankonsumsi",
            name: "dashboard_scanKonsumsi",
            component: dashboard_scanKonsumsi,
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/authenticated', { headers: headers }).then((response) => {
                    store.commit('userAuthenticated', {
                        isLogin: true,
                        user: response.data
                    });


                    next();
                }).catch(() => {
                    store.commit('userAuthenticated', {
                        isLogin: false,
                        user: {}
                    });


                    next({ name: 'login' });
                });
            }

        },
        {
            path: "/backend/scanphoto",
            name: "dashboard_scanPhoto",
            component: dashboard_scanPhoto,
            beforeEnter: (to, form, next) => {
                const headers = {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + store.state.token,
                };

                axios.get('/api/authenticated', { headers: headers }).then((response) => {
                    store.commit('userAuthenticated', {
                        isLogin: true,
                        user: response.data
                    });


                    next();
                }).catch(() => {
                    store.commit('userAuthenticated', {
                        isLogin: false,
                        user: {}
                    });


                    next({ name: 'login' });
                });
            }

        },
        {
            path: "/dorprize/slot",
            name: "slot",
            component: slot

        },
        {
            path: "/dorprize/motor",
            name: "motor",
            component: motor

        },
        {
            path: "/dorprize/nocheckin",
            name: "nocheckin",
            component: nocheckin

        }

    ]
}