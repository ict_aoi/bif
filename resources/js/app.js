import Vuex from 'vuex';
import VueRouter from 'vue-router';
import routes from './routes';
import store from './store';

import BlockUI from 'vue-blockui';
import VueGoodTablePlugin from 'vue-good-table';
import 'vue-good-table/dist/vue-good-table.css'
import { Datetime } from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'
import { Settings } from 'luxon'
import moment from 'moment';
// import Swal from 'sweetalert2/dist/sweetalert2.js';

// import 'sweetalert2/src/sweetalert2.scss';

 
//Settings.defaultLocale = 'ID';

window.Vue = require('vue').default;
window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.withCredentials = true;

let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

Vue.config.debug = false;
Vue.config.devtools = false;


Vue.component('rundown-event', require('./components/frontend/RundownEvent.vue').default);
Vue.component('menu-frontend', require('./components/frontend/includes/Menu.vue').default);
Vue.component('footer-frontend', require('./components/frontend/includes/Footer.vue').default);

Vue.component('header-backend', require('./components/backend/includes/header.vue').default);
Vue.component('menu-backend', require('./components/backend/includes/menu.vue').default);
Vue.component('footer-backend', require('./components/backend/includes/Footer.vue').default);
Vue.component('breadcrumbs-backend', require('./components/backend/includes/breadcrumbs.vue').default);
Vue.component('vue-datetime', Datetime);


Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(BlockUI);
Vue.use(VueGoodTablePlugin);


Vue.filter('formatDateTime', function(value) {
    if (value) {
        return moment(String(value)).format('DD/MMMM/YYYY HH:mm:ss')
    }
});

const app = new Vue({
    el: '#app',
    router: new VueRouter(routes),
    store: store
});

