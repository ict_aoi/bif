import Vuex from 'vuex';
import Vue from 'vue'

Vue.use(Vuex);
const store = new Vuex.Store({
    state: {
        tokenFront: window.localStorage.getItem('tokenFront'),
        token: window.localStorage.getItem('token'),
        selectedCrumb: '',
        selectedFrontPage: '',
        crumbs: [],
        showLoginDialog: false,
        isUserLogin: false,
        isLogin: false,
        user: {},
    },
    mutations: {
        authenticate(state, payload) {
            state.token = payload.token;
            localStorage.setItem('token', payload.token);
        },
        crumb(state, payload) {
            state.selectedCrumb = payload.selectedCrumb;
            state.crumbs = payload.crumbs;
        },
        setFrontPage(state, payload) {
            state.selectedFrontPage = payload.selectedFrontPage;
        },
        frontAuthenticated(state, payload) {
            state.showLoginDialog = payload.showLoginDialog;
            state.isUserLogin = payload.isUserLogin;
            state.tokenFront = payload.tokenFront;
            localStorage.setItem('tokenFront', payload.tokenFront);
        },
        userAuthenticated(state, payload) {
            state.user = payload.user;
            state.isLogin = payload.isLogin;
        },
    },
    actions: {
        islogin: ({ commit }, { isLogin }) => commit('islogin', isLogin)
    }
})

export default store;