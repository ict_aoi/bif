<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <meta name="description" content="BIF XIX - Together we rise!" itemprop="description">
        <meta name="originalTitle" content="BIF 19">
        <meta name="keywords" content="bif, bina busana internusa, bbi">
        <meta name="author" content="ICT 2023">

        <meta property="og:url" content="http://bif19.bbi-apparel.com/">
        <meta property="og:image" content="http://bif19.bbi-apparel.com/logo">
        <meta property="og:image:type" content="image/png">
    	<meta property="og:image:width" content="650">
    	<meta property="og:image:height" content="366">
        <meta property="og:description" content="BIF XIX - Together we rise!">
        <meta property="og:title" content="BIF 19">
        <meta property="og:type" content="website">
        <meta property="og:locale" content="en_ID">

        <title>BIF | Bina Busana Improvement Forum</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <link rel="bif-icon" href="{{ asset('images/favicon.ico') }}">
        <link type="image/x-icon" rel="shortcut icon" href="http://bif19.bbi-apparel.com/logo">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/landing_page.css') }}">
        <script src="{{ asset('js/frontend_header.js') }}"></script>
       
        @yield('styles')
        
    </head>

    <body class="animate-page" data-spy="scroll" data-target="#navbar" data-offset="100">
        <div class="preloader"></div>
        <div id="app">
            @yield('content')
        </div>

        

        <a href="#top" class="back_to_top"><img src="{{ asset('images/back_to_top.png')}}" alt="back to top"></a>

        <script src="{{ mix('js/jquery.min.js') }}"></script>
        <script src="{{ mix('js/bootstrap.min.js') }}"></script>
        <script src="{{ mix('js/countdown.js') }}"></script>
        <script src="{{ mix('js/wow.js') }}"></script>
        <script src="{{ mix('js/slick.js') }}"></script>
        <script src="{{ mix('js/magnific-popup.js') }}"></script>
        <script src="{{ mix('js/appear.js') }}"></script>
        <script src="{{ mix('js/count-to.js') }}"></script>
        <script src="{{ mix('js/nicescroll.js') }}"></script>
        <script src="{{ mix('js/sweetalert2.js') }}"></script>
        <script src="{{ mix('js/main.js') }}"></script>
        @yield('scripts')
       </body>
</html>