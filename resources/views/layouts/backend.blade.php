<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="BIF ADMIN.">
    <meta name="keywords" content="bif, bina busana internusa, bbi">
    <meta name="author" content="ICT 2021">

    <meta property="og:url" content="http://bif19.bbi-apparel.com/">
    <meta property="og:image" content="http://bif19.bbi-apparel.com/logo">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="650">
    <meta property="og:image:height" content="366">
    <meta property="og:description" content="BIF App">
    <meta property="og:title" content="BIF | Bina Busana Improvement Forum">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="en_ID">

    <title>BIF | Bina Busana Improvement Forum</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link type="image/x-icon" rel="shortcut icon" href="http://bif19.bbi-apparel.com/logo">

    <link rel="bif-icon" href="{{ asset('images/favicon.ico') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/templete_admin.css') }}">
    @yield('styles')

</head>

<body class="vertical-layout vertical-menu-modern navbar-sticky footer-static" data-open="click"
    data-menu="vertical-menu-modern" data-col="'2-columns">
    <div id="app">
        <div class="header-navbar-shadow"></div>
        <header-backend></header-backend>
        {{-- @include('includes.backend.header')
            @include('includes.backend.menu') --}}
        <menu-backend></menu-backend>


        <div class="app-content content">
            <div class="content-overlay"></div>
            <div class="content-wrapper">
                <breadcrumbs-backend></breadcrumbs-backend>
                <div class="content-body">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <footer-backend></footer-backend>
    {{-- @include('includes.backend.footer') --}}
    <script src="{{ mix('js/templete_admin.js') }}"></script>
    @yield('scripts')
</body>

</html>
