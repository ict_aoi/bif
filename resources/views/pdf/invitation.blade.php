<!DOCTYPE html>
<html>
<style>
    body {
        background-color: #f5f5f5;
        font-family: 'Montserrat', sans-serif;
    }


    img {
        /* margin-top: 30px; */
        max-width: 100%;
    }

    #download-btn {
        background-color: #f39c12;
        border: none;
        border-radius: 5px;
        color: #fff;
        cursor: pointer;
        display: block;
        font-size: 20px;
        margin-top: 10px;
        padding: 10px 20px;
        text-align: center;
        text-decoration: none;
        transition: background-color 0.3s ease;
    }

    #download-btn:hover {
        background-color: #e67e22;
    }



    .bg-image {
        background-color: rgba(0, 0, 0, 0.5);
    }
</style>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description"
        content="BIF XIX - Together we rise!"
        itemprop="description">
    <meta name="originalTitle" content="BIF 19">
    <meta name="keywords" content="bif, bina busana internusa, bbi">
    <meta name="author" content="ICT 2021">

    <meta property="og:url" content="http://bif19.bbi-apparel.com/">
    <meta property="og:image" content="http://bif19.bbi-apparel.com/logo">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="650">
    <meta property="og:image:height" content="366">
    <meta property="og:description"
        content="BIF XIX - Together we rise!">
    <meta property="og:title" content="BIF 19">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="en_ID">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="bif-icon" href="{{ asset('images/favicon.ico') }}">
    <link type="image/x-icon" rel="shortcut icon" href="http://bif19.bbi-apparel.com/logo">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <title>BIF | Invitation Card</title>
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/landing_page.css') }}">
    <script src="{{ asset('js/frontend_header.js') }}"></script>

</head>

<body class="animate-page bg-image" data-target="#navbar" data-offset="100"
    style="margin-top:0px;height:100vh;overflow: hidden;">

    <div class="container2">
        {{-- <div class="card border" id="gambar" style="border-radius:10px; font-weight: 700">
            <div class="barcode">
                <div class="img_barcode">
                    <center>
                        <img class="image_barcode" src="{{ DNS2D::getBarcodePNGPath($enc_nik, 'QRCODE') }}"
                            alt="Barcode">
                    </center>

                </div>
            </div>
            <div class="text-center">
                <table class="table_css">
                    <tr>
                        <td colspan="2" style="background-color: red; color:white; border-radius: 10px 10px 0 0;"
                            class="text-center">{{ ucwords(strtolower($data->name)) }}<br> ({{ $data->nik }})</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="font_sizeDoorprize text-center">{{ $data->nomor }}<img
                                src="{{ asset('images/gambar/emoji/' . substr($data->lottery_number, -1) . '.png') }}"
                                id="nomor" class="image_doorprize" alt=""></td>
                    </tr>

                </table>
            </div>
        </div> --}}
        <div class="card border" id="gambar" style="border-radius:10px; font-weight: 700">
            <img src="{{ asset('/images/back_card_mobile.png') }}" alt="" class="gambar_card">
            <div class="barcode">
                <img class="gambar_barcode" src="{{ DNS2D::getBarcodePNGPath($enc_nik, 'QRCODE', 5, 5) }}"
                    alt="Barcode">
            </div>
            <table class="table_isi">
                <tr>
                    <td colspan="2" style="background-color: #FF0000; color:#FFFFFF; border-radius: 10px 10px 0 0;"
                        class="text-center">{{ ucwords(strtolower($data->name)) }}<br> ({{ $data->nik }})</td>
                </tr>
                <tr>
                    <td colspan="2" class="font_sizeundian text-center"
                        style="background-color: #FFFFFF; border-radius:0 0 10px 10px">{{ $data->nomor }}<img
                            src="{{ asset('images/gambar/emoji/' . substr($data->lottery_number, -1) . '.png') }}"
                            id="nomor" class="gambar_undian" style="" alt=""></td>
                </tr>
            </table>
        </div>
        <a href="#!" id="download-btn">Download</a>
    </div>




    <script src="{{ mix('js/jquery.min.js') }}"></script>
    <script src="{{ mix('js/bootstrap.min.js') }}"></script>
    <script src="{{ mix('js/countdown.js') }}"></script>
    <script src="{{ mix('js/wow.js') }}"></script>
    <script src="{{ mix('js/slick.js') }}"></script>
    <script src="{{ mix('js/magnific-popup.js') }}"></script>
    <script src="{{ mix('js/appear.js') }}"></script>
    <script src="{{ mix('js/count-to.js') }}"></script>
    <script src="{{ mix('js/nicescroll.js') }}"></script>
    <script src="{{ mix('js/sweetalert2.js') }}"></script>
    <script src="{{ mix('js/main.js') }}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/dom-to-image/2.6.0/dom-to-image.min.js"></script>  --}}
    <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.3.2/dist/html2canvas.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.5/FileSaver.min.js"></script>
    {{-- <script>
        $(document).ready(function() {
            $(window).on('load', function() {
                // Mendapatkan elemen gambar dengan jQuery
                const image = $('#gambar');
                // Mendapatkan elemen button download dengan jQuery
                const downloadBtn = $('#download-btn');
                downloadBtn.on('click', function() {
                    // Mengubah elemen gambar menjadi canvas menggunakan HTML2Canvas
                    html2canvas(image[0]).then(function(canvas) {
                        // Mengubah canvas menjadi blob
                        canvas.toBlob(function(blob) {
                            let name = "{{ $data['nik'] }}";
    // Menyimpan blob sebagai file PNG
    saveAs(blob, name + '.png');
    });
    });
    });

    // Menambahkan event listener pada button download
    });
    });
    </script> --}}
    <script>
        $(document).ready(function() {
            $(window).on('load', function() {
                let img = '{{ asset('/images/logo.png') }}';
                Swal.fire({
                    title: 'Note!',
                    text: 'Perhatikan nomor doorprize yang tertera, nomor akan sama dengan peserta lain, namun berbeda dengan icon yang muncul pada masing-masing invitation card!',
                    imageUrl: img,
                    imageWidth: 150,
                    // imageHeight: 200,
                    imageAlt: 'Custom image',
                })

                // Mendapatkan elemen gambar dengan jQuery
                const image = $('#gambar');

                // Mendapatkan elemen button download dengan jQuery
                const downloadBtn = $('#download-btn');
                // downloadBtn.on('click', function() {
                //     // Mengubah elemen gambar menjadi canvas menggunakan HTML2Canvas

                //     html2canvas(image[0]).then(function(canvas) {
                //         // Mengambil konteks dari canvas
                //         const ctx = canvas.getContext('2d');

                //         // Mendapatkan data gambar dalam bentuk URL
                //         const dataURL = canvas.toDataURL('image/jpeg', 0.9);

                //         // Membuat elemen gambar baru dengan bentuk radius
                //         const imgRadius = new Image();
                //         imgRadius.onload = function() {
                //             // Membuat canvas baru dengan ukuran yang sama dengan gambar
                //             const canvasRadius = document.createElement('canvas');
                //             canvasRadius.width = imgRadius.width;
                //             canvasRadius.height = imgRadius.height;
                //             const ctxRadius = canvasRadius.getContext('2d');

                //             // Menggambar gambar dengan bentuk radius ke canvas baru
                //             ctxRadius.beginPath();
                //             ctxRadius.moveTo(40, 0);
                //             ctxRadius.lineTo(imgRadius.width - 10, 0);
                //             ctxRadius.quadraticCurveTo(imgRadius.width, 0, imgRadius
                //                 .width, 10);
                //             ctxRadius.lineTo(imgRadius.width, imgRadius.height - 10);
                //             ctxRadius.quadraticCurveTo(imgRadius.width, imgRadius
                //                 .height, imgRadius.width - 10, imgRadius.height);
                //             ctxRadius.lineTo(10, imgRadius.height);
                //             ctxRadius.quadraticCurveTo(0, imgRadius.height, 0, imgRadius
                //                 .height - 10);
                //             ctxRadius.lineTo(0, 10);
                //             ctxRadius.quadraticCurveTo(0, 0, 10, 0);
                //             ctxRadius.closePath();
                //             ctxRadius.clip();
                //             ctxRadius.drawImage(imgRadius, 0, 0, imgRadius.width,
                //                 imgRadius.height);

                //             // Mengubah canvas dengan gambar yang memiliki bentuk radius menjadi blob
                //             canvasRadius.toBlob(function(blob) {
                //                 let name = "{{ $data['nik'] }}";
                //                 // Menyimpan blob sebagai file PNG
                //                 saveAs(blob, name + '.jpeg');
                //             });
                //         };
                //         imgRadius.src = dataURL;
                //     });
                // });
                downloadBtn.on('click', function() {
                    html2canvas(image[0], {
                        scale: 3
                    }).then(
                        canvas => {
                            var a = document.createElement('a');
                            a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg",
                                "image/octet-stream");
                            a.download = '{{ $data['nik'] }}.jpg';
                            a.click();
                        });
                })

                // Menambahkan event listener pada button download
            });
        });
    </script>
    @yield('scripts')
</body>

</html>
