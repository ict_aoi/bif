<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Table Barcode</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</head>

<body>

    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h1>Table Barcode</h1>

                    </div>
                    <div class="col-6">
                        {{-- <button class="btn btn-primary" style="float: right" onclick="sendAll()">Send All</button> --}}

                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="sewingTable">
                        <thead>
                            <tr>
                                <td class="text-center">No</td>
                                <td class="text-center" style="min-width: 150px ">Name</td>
                                <td class="text-center" style="min-width: 150px ">No Hp</td>
                                <td class="text-center" style="min-width: 150px ">Email</td>
                                <td class="text-center" style="min-width: 150px ">Barcode</td>
                                <td class="text-center" style="min-width: 150px ">Card</td>
                                <td class="text-center" style="min-width: 150px ">Last Send</td>
                                <td class="text-center" style="min-width: 150px ">Send</td>
                                <td class="text-center" style="min-width: 150px ">Download</td>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>

</body>
<script>
    var table;
    $(document).ready(function() {
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            responsive: true,
            orderable: true,
            serverSide: true,
            deferRender: true,
            "scrollX": true,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span class="me-3">Filter:</span> <div class="form-control-feedback form-control-feedback-end flex-fill">_INPUT_<div class="form-control-feedback-icon"><i class="ph-magnifying-glass opacity-50"></i></div></div>',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span class="me-1">Show:</span> _MENU_',
                paginate: true
            }
        });
        table = $('#sewingTable').DataTable({
            ajax: {
                url: "{{ route('barcode.data') }}",
                data: function(d) {

                },
                beforeSend: function() {

                },
                complete: function() {}
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'nik',
                    name: 'nik'
                },
                {
                    data: 'email',
                    name: 'email'
                },

                {
                    data: 'barcode',
                    name: 'barcode'
                },
                {
                    data: 'card',
                    name: 'card'
                },

                {
                    data: 'is_send',
                    name: 'is_send'
                },

                {
                    data: 'send_mail',
                    name: 'send_mail',
                    class: 'text-center',
                },
                {
                    data: 'download',
                    name: 'download'
                }

            ]
        });
    })

    function sendMail(url) {
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function() {
                $.blockUI();
            },
            complete: function() {

            },
            success: function(response) {
                $.unblockUI();
                Swal.fire({
                    title: "Good job!",
                    text: "",
                    icon: "success"
                });
                table.ajax.reload();
            },
            error: function(response) {
                Swal.fire({
                    title: "Error!",
                    text: response.responseText,
                    icon: "error"
                });
            }
        });
    }
    function sendAll() {
        $.ajax({
            url: "{{route('barcode.sendAll')}}",
            type: "GET",
            beforeSend: function() {
                $.blockUI();
            },
            complete: function() {

            },
            success: function(response) {
                $.unblockUI();
                Swal.fire({
                    title: "Good job!",
                    text: "",
                    icon: "success"
                });
                table.ajax.reload();
            },
            error: function(response) {
                Swal.fire({
                    title: "Error!",
                    text: response.responseText,
                    icon: "error"
                });
            }
        });
    }
</script>

</html>
