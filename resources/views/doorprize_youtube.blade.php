<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Doorprize BIF19</title>
    <link rel="bif-icon" href="{{ asset('images/favicon.ico') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/landing_page.css') }}">
    <style>
        .col-lg-12 {
            left: 19%;
            top: 50%;
        }

        .container {
            top: 50%;
            left: 50%;
            position: absolute;
            transform: translate(-50%, -50%);
        }

        .input_text {
            font-size: 25px;
            max-width: 200px;
            height: 100px;
            background: transparent;
            border: none;
            background-image: url("/images/panah_kanan.png");
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            /* background-color:#f8e9d6;  */
            color: #28495a;
            /* border: 2px #f8e9d6; */
            font-weight: 700;
            text-align: center;
            margin-top: 3%;
        }

        .number_doorprize {
            font-size: 50px;
            background-color: #28495a;
            color: #f8e9d6;
            width: 200px;
            text-align: center;
            border-radius: 10px
        }

        .btn_play {
            background-color: #28495a;
            color: #f8e9d6;
            position: fixed;
            margin-top: 88vh;
            margin-left: 10vw;

        }
    </style>

</head>

<body
    style="margin-top:0px;height:100vh;overflow: hidden; background-image:url('/images/undian.jpg'); background-repeat:no-repeat; background-size:cover; background-position:center;">
    {{-- <input type="checkbox" name="" id="peserta"> --}}
    <div class="container">
        <audio id="drum" src="{{ asset('/sound/drum.mp3') }}" type="audio/mpeg">
        </audio>
        <audio id="ces" src="{{ asset('/sound/ces.mp3') }}" type="audio/mpeg">
        </audio>
        <div class="row">
            @for ($i = 1; $i <= $idx; $i++)
                <div class="col-lg-12">
                    <div class="col-lg-3" style="text-align: right; margin-top: 3%;">
                        <input type="text" class="input_text" name="" id="gift_{{ $i }}"
                            value="{{ $i }}" style="">
                    </div>
                    <div class="col-lg-9" style="margin-top: 3%;">
                        <p class="number_doorprize" id="number_{{ $i }}">----</p>
                    </div>
                </div>
            @endfor
        </div>
    </div>
    <button id="play_number" class="btn btn_play">Spin</button>
</body>
<script src="{{ mix('js/jquery.min.js') }}"></script>
<script src="{{ mix('js/bootstrap.min.js') }}"></script>
<script src="{{ mix('js/countdown.js') }}"></script>
<script src="{{ mix('js/wow.js') }}"></script>
<script src="{{ mix('js/slick.js') }}"></script>
<script src="{{ mix('js/magnific-popup.js') }}"></script>
<script src="{{ mix('js/appear.js') }}"></script>
<script src="{{ mix('js/count-to.js') }}"></script>
<script src="{{ mix('js/nicescroll.js') }}"></script>
<script src="{{ mix('js/sweetalert2.js') }}"></script>
<script src="{{ mix('js/main.js') }}"></script>

<script>
    $(document).ready(function() {
        var idx = '{{ $idx }}';
        var playing = false;
        var intervals = [];
        var no_pemenang = [];
        var gift_pemenang = [];
        var drum, ces;
        drum = document.getElementById("drum");
        ces = $('#ces')[0];
        drum.load();
        ces.load();
        $('#play_number').on('click', function() {
            if (playing == false) {
                no_pemenang = [];
                let peserta_check = $('#peserta');
                let status_peserta = 1;
                // if (peserta_check.prop('checked')) {
                //     status_peserta = 1;
                // }
                $.ajax({
                    url: '{{ route('dorprize_number_youtube') }}', // URL get nomor undian Youtube
                    type: "GET",
                    data: {
                        peserta: status_peserta,
                        jumlah: idx
                    },
                    success: function(response) {
                        console.log(response);
                        if (response.success != true) {
                            // Swal.fire('Ups!', 'Participant Not Found', 'info');
                            alert('Participant Not Found');
                            return;
                        }
                        $('#play_number').text('Stop');

                        $(response.number).each(function(index, value) {
                            no_pemenang.push(value);
                        })

                        for (let i = 1; i <= idx; i++) {
                            let interval = setInterval(() => {
                                let random_number = Math.floor(Math.random() *
                                    9000) + 1000;
                                let lastDigit = random_number % 10;
                                $('#number_' + i).html(Math.floor(random_number /
                                        10) +
                                    '<img src="/images/gambar/doorprize/' +
                                    lastDigit +
                                    '.png" style="max-height:50px">');
                            }, 100);
                            intervals.push(interval);
                        }
                        playing = true;
                        play_drum();

                    },
                    error: function(response) {
                        Swal.fire('Error!', 'System Error!!', 'error');
                    }
                });


            } else {
                $('#play_number').text('Spin');
                gift_pemenang = [];
                intervals.forEach(interval => clearInterval(interval));
                play_ces();
                stop_drum();
                $('.input_text').each(function(index, value) {
                    gift_pemenang.push(value.value);
                })

                $.ajax({
                    url: '{{ route('dorprize_gift_youtube') }}',
                    type: "GET",
                    data: {
                        gift: gift_pemenang,
                        pemenang: no_pemenang
                    },
                    success: function(response) {
                        if (response.success != true) {
                            Swal.fire('Ups!', 'Not save', 'info');
                        }
                        // $(no_pemenang).each(function(index, value) {
                        //     no_pemenang.push(value);
                        // })
                        
                        
                        // console.log(q);
                    },
                    error: function(response) {
                        Swal.fire('Error!', 'System Error!!', 'error');
                    }
                });
                for (let i = 1; i <= idx; i++) {
                    let lastDigit_pemenang = no_pemenang[i - 1] % 10;
                    $('#number_' + i).html(Math.floor(no_pemenang[i - 1] / 10) +
                        '<img src="/images/gambar/doorprize/' + lastDigit_pemenang +
                        '.png" style="max-height:50px">');
                }
                playing = false;
            }
        });
    });

    function play_drum() {


        drum.play().catch(function(error) {
            console.log(error);
        });
    }

    function stop_drum() {
        drum.pause();
        drum.currentTime = 0;
    }

    function play_ces() {

        ces.play().catch(function(error) {
            console.log(error);
        });
    }

    function stop_ces() {
        ces.pause();
        ces.currentTime = 0;
    }
</script>


</html>
