<table style="width: 100%; max-width: 600px; margin: 0 auto; background-color: #f6f6f6; border-collapse: collapse;">
    <tr>
        <td style="padding: 5px; text-align:center ">
            <img src="https://i.ibb.co.com/rdqVn2C/head-email.jpg" alt="Logo Perusahaan" style="">
        </td>
    </tr>
    <tr>
        <td style="padding: 30px; text-align: center; background-color: #ffffff;">
            <center>
                <p style="font-size: 18px; text-align:center">"You are Invited to <b>BIF XIX - Together we rise </b>
                    2024!"
                </p>
                <table style="text-align:left;font-size:18px">
                    <tr>
                        <td colspan="3"><strong>Here are the event details
                                :</strong></td>
                    </tr>
                    <tr>
                        <td><strong> Date </strong></td>
                        <td><strong> : </strong></td>
                        <td><strong> 13th July 2024 </strong></td>
                    </tr>
                    <tr>
                        <td><strong> Time </strong></td>
                        <td><strong> : </strong></td>
                        <td><strong> 06.00 A.M - End </strong></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top"><strong> Venue </strong></td>
                        <td style="vertical-align: top"><strong> : </strong></td>
                        <td><strong> PT. Apparel One Indonesia 3
                            </strong></td>
                    </tr>
                </table>
                <br>
                <p>
                    Each participants has been assigned a unique registration barcode for entry to the event. Please
                    find your individual barcode on link below. <br>
                    Kindly display this barcode on the registration area. <br><br>
                    We are looking forward to seeing you in Semarang and have a fruitful sharing and discussion.
                </p>
                <a download="{{ $data->name }}.png" href="{{ route('invitationCard', $data->nik) }}"
                    title="{{ $data->name }}"
                    style="display: inline-block; padding: 15px 40px; background-color: #5c5c5c; color: #ffffff; text-decoration: none; font-size: 18px;">Download
                    Registration Card</a>
            </center>
        </td>
    </tr>
</table>