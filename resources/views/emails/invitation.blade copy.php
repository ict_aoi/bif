@component('mail::message')
    <table style="width: 100%; max-width: 600px; margin: 0 auto; background-color: #f6f6f6; border-collapse: collapse;">
        <tr>
            <td style="padding: 5px; ">
                <img src="https://i.ibb.co/JccLGKh/logo.png" alt="Logo Perusahaan" style="max-width: 200px; max-height:80px;text-align: left;">
                <img src="https://i.ibb.co/WDTnZkS/logo-bif18.png" alt="Logo Perusahaan" style="max-width: 200px; max-height:80px;float: right;">
            </td>
        </tr>
        <tr>
            <td style="padding: 30px; text-align: center; background-color: #ffffff;">
                <center>
                    <h1 style="font-size: 32px; margin-bottom: 30px; text-align:center">THANKYOU</h1>
                    <p style="font-size: 18px; text-align:center">{{ ucwords(strtolower($data['name'])) }}, You're successful
                        registered.</p>
                    <p style="font-size: 18px; margin-bottom: 30px; text-align:center">
                        <strong>Binabusana Improvement Forum</strong><br>
                        <strong>July, 8th 2023</strong><br>
                        <strong>8 AM - 1 PM</strong><br>
                        <strong>Apparel One Indonesia 2</strong>
                    </p>
                    <h1 style="font-size: 28px; margin-bottom: 30px; text-align:center">LET'S GET OUR GROOVE ON!!!</h1>
                    <a download="{{$data->name}}.png" href="{{URL::to('/').$data->position}}" title="{{$data->name}}"
                        style="display: inline-block; padding: 15px 40px; background-color: #5c5c5c; color: #ffffff; text-decoration: none; font-size: 18px;">Download
                        Invitation Card</a>
                </center>
            </td>
        </tr>
        <tr>
            <td style="padding: 5px; text-align: center; background-color: #f6f6f6;">
                <img src="https://i.ibb.co/1GQc1T7/FOOTER-EMAIL.png"
                    style="height: 80px; width:500px" alt="">
            </td>
        </tr>
    </table>
@endcomponent
