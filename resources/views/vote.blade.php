<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        #chart-container {
            position: relative;
            height: 70vh;
            width: 70vw;
            top: 40%;
            left: 50%;
            transform: translate(-50%, -50%);
            /* opacity: 0.8; */
            overflow: hidden;
            background-color: white;
        }

        .gambar {
            background: url('/images/backgroup_non_mobile.png');
            background-color: rgba(0, 0, 0, 1.5);
            background-size: cover;
            /* opacity: 0.5; */
            width: 100%;
            height: calc(0.6 * 100vw);
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/landing_page.css') }}">

</head>


<body>
    <div class="gambar">
        <div id="chart-container"></div>
    </div>
    <audio id="drum" src="{{ asset('/sound/drum.mp3') }}" type="audio/mpeg">
    </audio>
    <audio id="ces" src="{{ asset('/sound/ces.mp3') }}" type="audio/mpeg">
    </audio>

</body>
<script src="https://fastly.jsdelivr.net/npm/echarts@5.4.2/dist/echarts.min.js"></script>
<script src="{{ mix('js/jquery.min.js') }}"></script>

<script>
    var dom = document.getElementById('chart-container');
    var myChart = echarts.init(dom, null, {
        renderer: 'canvas',
        useDirtyRect: false
    });
    var app = {};
    var option, dataLabels = [],
        interval, data = [],
        colors = [];
    durasi_animasi = 1000; ///milisecond
    time = 10000 //milisecond;
    var drum, ces;

    $(document).ready(function() {
        $(window).on('DOMContentLoaded', function() {
            drum = document.getElementById("drum");;
            ces = $('#ces')[0];
            drum.load();
            ces.load();
            $.ajax({
                url: "{{ route('vote.dataLabels') }}",
                success: function(response) {
                    dataLabels = response;
                    console.log(dataLabels);
                    myChart.setOption({
                        yAxis: {
                            data: dataLabels,
                        },
                    });
                    for (let i = 0; i < response.length; ++i) {
                        data.push(50);
                        colors.push(getRandomColor()); // Generate random color for each bar
                    }
                    myChart.setOption({
                        series: [{
                            type: 'bar',
                            data
                        }]
                    });


                    // dataLabels = data;
                },

            });


            $(document).one('click', function() {

                $(document).on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                });

                setTimeout(function() {
                    play_drum();
                }, durasi_animasi)
                interval = setInterval(function() {
                    run();
                }, durasi_animasi);
                //settime out vote
                setTimeout(function() {
                    clearInterval(interval);
                    $.ajax({
                        url: "{{ route('vote.getData') }}",
                        success: function(response) {
                            // console.log(data);
                            for (var i = 0; i < data.length; ++i) {
                                data[i] = response[i];
                            }
                            myChart.setOption({
                                series: [{
                                    type: 'bar',
                                    data
                                }]
                            });
                        },
                        complete: function() {
                            setTimeout(function() {
                                stop_drum();
                                play_ces();

                            }, durasi_animasi)

                        }
                    });
                }, time);
            })

            option = {
                xAxis: {
                    max: '100'
                },
                title: {
                    left: 'center',
                    text: 'Hasil vote foto / video chalange'
                },
                yAxis: {
                    type: 'category',
                    // data: ['A', 'B', 'C', 'D', 'E'],
                    data: dataLabels,
                    inverse: true,
                    animationDuration: 300,
                    animationDurationUpdate: 300,
                    // max: 6 // only the largest 3 bars will be displayed
                },
                series: [{
                    realtimeSort: true,
                    name: '',
                    type: 'bar',
                    data: data,
                    label: {
                        show: true,
                        position: 'right',
                        valueAnimation: true
                    },
                    itemStyle: {
                        color: function(params) {
                            return colors[params
                                .dataIndex]; // Set color based on the index of the bar
                        }
                    }
                }],
                legend: {
                    show: true
                },
                animationDuration: 0,
                animationDurationUpdate: durasi_animasi,
                animationEasing: 'linear',
                animationEasingUpdate: 'linear'
            };



            if (option && typeof option === 'object') {
                myChart.setOption(option);
            }

            function getRandomColor() {
                var letters = '0123456789ABCDEF';
                var color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;
            }
        });

    });


    function run() {
        for (var i = 0; i < data.length; ++i) {
            data[i] = Math.floor(Math.random() * 91) + 10;
        }
        myChart.setOption({
            series: [{
                type: 'bar',
                data
            }]
        });
    }

    function play_drum() {


        drum.play().catch(function(error) {
            console.log(error);
        });
    }

    function stop_drum() {
        drum.pause();
        drum.currentTime = 0;
    }

    function play_ces() {

        ces.play().catch(function(error) {
            console.log(error);
        });
    }

    function stop_ces() {
        ces.pause();
        ces.currentTime = 0;
    }
</script>

</html>
