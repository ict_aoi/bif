<!DOCTYPE html>
<!--html-- lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
   
    @foreach ($score as $sc)
<input type="text" name="team{{ $sc->team }}" class="teams" id="team{{ $sc->team }}" value="{{ $sc->score }}" id="">
@endforeach
    <a href="#" id="simpan">Save</a>
</body>
<script src="{{ mix('js/jquery.min.js') }}"></script>
<script src="https://cdn.socket.io/4.6.0/socket.io.min.js"
    integrity="sha384-c79GN5VsunZvi+Q/WObgk2in0CbZsHnjEqvFxC5DxHn9lTfNce2WW6h2pH6u/kF+" crossorigin="anonymous">
</script>

<script>
    // connect ke socket disini,semua client harus connect
    const socket = io("https://mdware.bbi-apparel.com", {
        path: '/socket'
    });
</script>
<script>
    $(document).ready(function() {
        // $('#team1').text(200);

        $('#simpan').on('click', function() {
            // console.log('click');
            console.log($('.teams'));
            let arr = [];

            $.each($('.teams'), function(key, val) {
                arr.push(val.value);
            })
            $.ajax({
                url: "{{ route('score.update') }}",
                dataType: 'JSON',
                data: {
                    teams: arr,
                },
                success: function(data) {
                    console.log(data);
                    if (socket.connected) {
                        console.log('im here');
                        socket.emit('addScore');
                    }

                },
            });
        })

    });
</script>

</!--html-->

<!DOCTYPE html>
<html>

<head>
    <title>Quiz Scores</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Quiz Scores</h3>
                    </div>
                    <div class="card-body">
                        @foreach ($score as $sc)
                            <div class="form-group">
                                <label for="team1">Team {{ $sc->team }} Score:</label>
                                <input type="text" class="form-control teams" id="{{ $sc->id }}"
                                    value="{{ $sc->score }}">
                            </div>
                        @endforeach
                        {{-- <div class="form-group">
                            <label for="team1">Team 1 Score:</label>
                            <input type="text" class="form-control" id="team1" value="85" readonly>
                        </div>
                        <div class="form-group">
                            <label for="team2">Team 2 Score:</label>
                            <input type="text" class="form-control" id="team2" value="72" readonly>
                        </div>
                        <div class="form-group">
                            <label for="team3">Team 3 Score:</label>
                            <input type="text" class="form-control" id="team3" value="93" readonly>
                        </div>
                        <div class="form-group">
                            <label for="team4">Team 4 Score:</label>
                            <input type="text" class="form-control" id="team4" value="68" readonly>
                        </div> --}}
                        <div class="text-center">
                            <a href="#" class="btn btn-primary" id="simpan">Simpan</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="{{ mix('js/jquery.min.js') }}"></script>
<script src="https://cdn.socket.io/4.6.0/socket.io.min.js"
    integrity="sha384-c79GN5VsunZvi+Q/WObgk2in0CbZsHnjEqvFxC5DxHn9lTfNce2WW6h2pH6u/kF+" crossorigin="anonymous">
</script>

<script>
    // connect ke socket disini,semua client harus connect
    const socket = io("https://mdware.bbi-apparel.com", {
        path: '/socket'
    });
</script>
<script>
    $(document).ready(function() {
        // $('#team1').text(200);

        $('#simpan').on('click', function() {
            // console.log('click');
            console.log($('.teams'));
            let arr = [];
            // let tamp = [];
            $.each($('.teams'), function(key, val) {
                var tamp = {};
                tamp['value'] = val.value;
                tamp['id'] = val.id;
                arr.push(tamp);
            })
            // console.log(arr);
            // return;
            $.ajax({
                url: "{{ route('score.update') }}",
                dataType: 'JSON',
                data: {
                    teams: arr,
                },
                success: function(data) {
                    console.log(data);
                    if (socket.connected) {
                        console.log('im here');
                        socket.emit('addScore');
                    }

                },
            });
        })

    });
</script>

</html>
