<!DOCTYPE html>
<!--html-- lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/landing_page.css') }}">
    <style>
        .tengah {
            top: 40%;
            left: 50%;
            transform: translate(-50%, -50%);
            position: absolute;
        }
    </style>
</head>


<body>
    <div class="bg-image" style="background-size: 100% 100%">
        <div>
            {{-- @foreach ($score as $key => $sc)
                <h1>score {{ $sc->team }} : <span id="team{{ $sc->team }}">{{ $sc->score }}</span></h1>
            @endforeach --}}
            <div class="container tengah" style="width: 90vw">
                <div class="row">
                    <div class="col-lg-3">
                        <div style="height:300px; background-color:white">

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div style="height:100px; background-color:white">

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div style="height:100px; background-color:white">

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div style="height:100px; background-color:white">

                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>

</body>

<script src="https://cdn.socket.io/4.6.0/socket.io.min.js"
    integrity="sha384-c79GN5VsunZvi+Q/WObgk2in0CbZsHnjEqvFxC5DxHn9lTfNce2WW6h2pH6u/kF+" crossorigin="anonymous">
</script>

<script>
    //connect ke socket disini,semua client harus connect
    const socket = io("https://mdware.bbi-apparel.com", {
        path: '/socket'
    });
</script>

<script src="{{ mix('js/jquery.min.js') }}"></script>
<script>
    $(document).ready(function() {
        // $('#team1').text(200);
        updateScore();
        socket.on('updateScore', () => {
            console.log('Update Notif by Socket.io');
            updateScore();
        });

        function updateScore() {
            $.ajax({
                url: "{{ route('score.get') }}",
                success: function(data) {
                    console.log(data);
                    if (socket.connected) {
                        socket.emit('agentAddComment');
                    }
                    for (let i = 0; i < data.length; i++) {
                        $('#team' + (data[i].team)).text(data[i].score);
                    }
                },
            });
        }

    });
</script>


</!--html-->
<!DOCTYPE html>
<html>

<head>
    <title>Scoreboard</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/landing_page.css') }}">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> --}}
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <style>
        html, body {
            margin: 0; 
            height: 100%; 
            overflow: hidden
        }
        .team-card {
            margin-bottom: 20px;
        }

        .team-card .card-body {
            min-width: 300px;
            background-color: #f8f9fa;
            text-align: center;
            border-radius: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1),
                0 4px 6px rgba(0, 0, 0, 0.1);
            /* background: url('/images/score.jpg'); */
            background-size:100% 100%;
            color: black; 
            padding-top:13%
        }

        .team-card .card-title {
            font-size: 54px;
            font-weight: 700;
            margin-bottom: 10px;
        }

        .team-card .card-subtitle {
            font-size: 48px;
            /* color: white; */
            margin-bottom: 10px;
        }

        .team-card .score {
            font-size: 100px;
            font-weight: bold;
            margin-left: -5%;
        }

        .container {
            margin-top: 0%;
            width: 90vw;
            padding-top:10%
        }
        body{
            overflow: hidden;
        }
        .text-center{
            color: black;
        }
        .bg-image{
            background: url('/images/back_scoring.png');
            background-size: cover;
        }
        
    </style>
</head>

<body class="bg-image" style="padding-bottom: 0;">
    <h1 class="text-center"></h1>
    <div class="container">
        <div class="row">
            @foreach ($score as $key => $sc)
            <div class="col-md-6 col-lg-6">
                <div class="team-card">
                    <div class="card-body" style="background: url('/images/score/{{$key+1}}.png'); background-size:cover;min-height:271px">
                        <input type="hidden" name="" id="id" value="{{$sc->id}}">
                        {{-- <h5 class="card-title">Team {{$sc->team}}</h5>
                        <h6 class="card-subtitle">Score</h6> --}}
                        <div class="score"><span id="team{{ $sc->id }}">{{$sc->score}}</span></div>
                    </div>
                </div>
            </div>
            @endforeach
            
            {{-- <div class="col-md-6 col-lg-3">
                <div class="team-card">
                    <div class="card-body">
                        <h5 class="card-title">Team B</h5>
                        <h6 class="card-subtitle">Score</h6>
                        <div class="score" id="team-b-score">0</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="team-card">
                    <div class="card-body">
                        <h5 class="card-title">Team C</h5>
                        <h6 class="card-subtitle">Score</h6>
                        <div class="score" id="team-c-score">0</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="team-card">
                    <div class="card-body">
                        <h5 class="card-title">Team D</h5>
                        <h6 class="card-subtitle">Score</h6>
                        <div class="score" id="team-d-score">0</div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</body>
<script src="https://cdn.socket.io/4.6.0/socket.io.min.js"
    integrity="sha384-c79GN5VsunZvi+Q/WObgk2in0CbZsHnjEqvFxC5DxHn9lTfNce2WW6h2pH6u/kF+" crossorigin="anonymous">
</script>

<script>
    //connect ke socket disini,semua client harus connect
    const socket = io("https://mdware.bbi-apparel.com", {
        path: '/socket'
    });
</script>

<script src="{{ mix('js/jquery.min.js') }}"></script>
<script>
    $(document).ready(function() {
        // $('#team1').text(200);
        updateScore();
        socket.on('updateScore', () => {
            console.log('Update Notif by Socket.io');
            updateScore();
        });

        function updateScore() {
            $.ajax({
                url: "{{ route('score.get') }}",
                success: function(data) {
                    console.log(data);
                    if (socket.connected) {
                        socket.emit('agentAddComment');
                    }
                    for (let i = 0; i < data.length; i++) {
                        $('#team' + (data[i].id)).text(data[i].score);
                        if(data[i].score < 0 ){
                            $("#team" + (data[i].id)).css("color", "red");
                        }else{
                            $("#team" + (data[i].id)).css("color", "black");
                        }
                    }
                },
            });
        }

    });
</script>

</html>
