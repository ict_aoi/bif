<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AksesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nik'       => '00000000',
            'name'      => 'sudinmoro',
            'email'     => 'maulanacomara@bbi-apparel.com',
            'password'  => Hash::make('milanisti88'),
            'is_admin'  => true,
            'email_verified_at' => Carbon::now()->todatetimestring(),
            'created_at' => Carbon::now()->todatetimestring(),
            'updated_at' => Carbon::now()->todatetimestring(),
        ]);


        DB::table('users')->insert([
            'nik'       => '11111111',
            'name'      => 'adincomara',
            'email'     => 'maulanacomara@bbi-apparel.com',
            'password'  => Hash::make('milanisti88'),
            'is_admin'  => false,
            'email_verified_at' => Carbon::now()->todatetimestring(),
            'created_at' => Carbon::now()->todatetimestring(),
            'updated_at' => Carbon::now()->todatetimestring(),
        ]);
    }
}
