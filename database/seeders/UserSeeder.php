<?php use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'nik'       => '180100515',
            'name'      => 'muhammad charis azwar',
            'email'     => 'charis.azwar@bbi-apparel.com',
            'password'  => Hash::make('password1'),
            'is_admin'  => true,
            'email_verified_at' => carbon::now()->todatetimestring(),
            'created_at' => carbon::now()->todatetimestring(),
            'updated_at' => carbon::now()->todatetimestring(),
        ]);

        DB::table('users')->insert([
            'nik'       => '170316003',
            'name'      => 'Ardhi Affandi',
            'email'     => 'ardhi.affandi@bbi-apparel.com',
            'password'  => Hash::make('password1'),
            'is_admin'  => true,
            'email_verified_at' => carbon::now()->todatetimestring(),
            'created_at' => carbon::now()->todatetimestring(),
            'updated_at' => carbon::now()->todatetimestring(),
        ]);

        DB::table('users')->insert([
            'nik'       => '171000309',
            'name'      => 'edi mulyono',
            'email'     => 'edi.mulyono@bbi-apparel.com',
            'password'  => Hash::make('password1'),
            'is_admin'  => false,
            'email_verified_at' => carbon::now()->todatetimestring(),
            'created_at' => carbon::now()->todatetimestring(),
            'updated_at' => carbon::now()->todatetimestring(),
        ]);
    }

}
