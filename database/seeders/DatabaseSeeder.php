<?php

namespace Database\Seeders;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

use Faker\Factory as Faker;
use UserSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*DB::table('users')->truncate();
        DB::table('users')->insert([
            'nik'       => '180100515',
            'name'      => 'muhammad charis azwar',
            'email'     => 'charis.azwar@bbi-apparel.com',
            'password'  => Hash::make('password1'),
            'is_admin'  => true,
        ]);

        DB::table('users')->insert([
            'nik'       => '170316003',
            'name'      => 'Ardhi Affandi',
            'email'     => 'ardhi.affandi@bbi-apparel.com',
            'password'  => Hash::make('password1'),
            'is_admin'  => true,
        ]);

        DB::table('users')->insert([
            'nik'       => '171000309',
            'name'      => 'edi mulyono',
            'email'     => 'edi.mulyono@bbi-apparel.com',
            'password'  => Hash::make('password1'),
            'is_admin'  => false,
        ]);*/

        

        // $faker = Faker::create('id_ID');
        // for($i = 1; $i <= 250; $i++){
        //     // insert data ke table siswa menggunakan Faker
        //     \DB::table('participants')->insert([
        //         'id' => $i,
        //         'nik' => $faker->randomNumber,
        //         'name' => $faker->name,
        //         'email' => $faker->email,
        //         'lottery_number' => $faker->randomNumber,
        //         'birth_day' => $faker->randomNumber,
        //         'registration_at' => Carbon::now(),
        //         'phone_number' => $faker->numberBetween(8000,9000)
        //     ]);
        // }
        $this->call(AksesSeeder::class);
    }
}
