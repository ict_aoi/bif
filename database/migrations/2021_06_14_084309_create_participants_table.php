<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('nik')->nullable();
            $table->string('name');
            $table->string('factory')->nullable();
            $table->string('circle')->nullable();
            $table->string('position')->nullable();
            $table->string('birth_day');
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->timestamp('registration_at')->nullable();
            $table->integer('checkin')->default(0);
            $table->integer('food')->default(0);
            $table->integer('photo')->default(0);
            $table->integer('peserta')->default(0);
            $table->string('lottery_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
