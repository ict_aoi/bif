<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_votes', function (Blueprint $table) {
            $table->char('id', 36)->primary();
            $table->string('file_name')->nullable();
            $table->string('thumb_name')->nullable();
            $table->string('description')->nullable();
            $table->string('mime_content_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_votes');
    }
}
