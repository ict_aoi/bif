<?php

return [
    'asset'         => storage_path() . '/app/asset',
    'pdf'         => storage_path() . '/app/pdf',
];
